package fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.Serializer;

/**
 * Created by jdeveaux on 26/09/2017.
 */

public interface ISerializer<T, ET> {
    ET serialize(T model);
    T deserialize(ET entity);
}
