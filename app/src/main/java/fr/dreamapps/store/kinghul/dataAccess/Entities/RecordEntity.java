package fr.dreamapps.store.kinghul.dataAccess.Entities;

import android.arch.persistence.room.Entity;

import java.util.Date;

import fr.dreamapps.store.kinghul.dataAccess.Entities.Abstract.AbstractEntity;

/**
 * Created by jdeveaux on 28/07/2017.
 */

@Entity
public class RecordEntity extends AbstractEntity  {
    public RecordEntity() {
        this.setCreationDate(new Date());
    }

    private String path;
    private boolean saved;


    public String getPath() {
        return path;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

}
