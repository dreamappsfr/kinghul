package fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface;

public interface ICallbackWithParam<T,P> {
    void invoke(T object, P parameter);
}
