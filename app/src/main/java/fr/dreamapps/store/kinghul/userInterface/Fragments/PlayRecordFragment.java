package fr.dreamapps.store.kinghul.userInterface.Fragments;


import android.arch.lifecycle.LiveData;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.dependancyInjection.Injectable;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;
import fr.dreamapps.store.kinghul.extensions.DateExtension;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PlayRecordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlayRecordFragment extends Fragment implements Injectable, SeekBar.OnSeekBarChangeListener {

    private RecordEntity recordEntity;
    private static String RECORD_ENTITY_KEY="RECORD_ENTITY_KEY";
    private LiveData<List<RecordLocalisationEntity>> localisations;

    private Handler mHandler = new Handler();

    @BindView(R.id.video_playing)
    VideoView videoView;

    @BindView(R.id.seek_bar)
    SeekBar seekBarVideo;

    @BindView(R.id.address_bar)
    TextView addressBar;

    @BindView(R.id.progress_timer)
    TextView progressTimer;

    @BindView(R.id.record_duration)
    TextView recordDuration;

    @BindView(R.id.play_control)
    ImageView playControl;

    @BindString(R.string.disabled_gps_information)
    String disabledGPSInformation;

    @Inject
    IRecordRepository recordRepository;

    Calendar c = Calendar.getInstance();
    private MediaMetadataRetriever retriever;

    public PlayRecordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param recordEntity Parameter 1.
     * @return A new instance of fragment PlayRecordFragment.
     */
    public static PlayRecordFragment newInstance(RecordEntity recordEntity) {
        PlayRecordFragment fragment = new PlayRecordFragment();
        Bundle args = new Bundle();
        args.putSerializable(RECORD_ENTITY_KEY, recordEntity);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recordEntity = (RecordEntity) getArguments().getSerializable(RECORD_ENTITY_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_play_record, container, false);
        ButterKnife.bind(this,view);

        if(recordEntity != null){
            localisations = recordRepository.retrieveLocalisationFor(recordEntity);
            localisations.observe(this,recordLocalisationEntities -> {
                this.updateProgressBar();
            });
            videoView.setVideoPath(recordEntity.getPath());
            videoView.requestFocus();
            updateProgressBar();
            seekBarVideo.setOnSeekBarChangeListener(this);
            this.retriever = new MediaMetadataRetriever();
            try {
                retriever.setDataSource(recordEntity.getPath());
            }catch (RuntimeException e){
                Log.e("PLAYRECORD", "onCreateView: recordEntity.getPath() : "+recordEntity.getPath(), e);
            }
            Bitmap bmp = retriever.getFrameAtTime(2 * 1000000, MediaMetadataRetriever.OPTION_CLOSEST);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bmp);
            videoView.setBackground(bitmapDrawable);


        }

        ((MainActivity) getActivity()).appBarLayoutVisibility(View.GONE);

        return view;
    }


    private void updateProgressBar() {
        mHandler.post(updateTimeTask);
    }

    private Runnable updateTimeTask = new Runnable() {
        public void run() {
            seekBarVideo.setMax(videoView.getDuration());
            seekBarVideo.setProgress(videoView.getCurrentPosition());
            mHandler.post(this);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        this.retriever = new MediaMetadataRetriever();
        retriever.setDataSource(recordEntity.getPath());
        if(videoView.isPlaying()){
            this.playControl.setImageResource(android.R.drawable.ic_media_pause);
        }
        else{
            this.playControl.setImageResource(android.R.drawable.ic_media_play);
            Bitmap bmp = retriever.getFrameAtTime(2 * 1000000, MediaMetadataRetriever.OPTION_CLOSEST);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bmp);
            videoView.setBackground(bitmapDrawable);
        }
        try {
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            recordDuration.setText(DateExtension.milliSecondsToTimer(timeInMillisec));
            retriever.release();
        }
        catch (NumberFormatException e){
            Timber.e(e);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekbar, int progress,boolean fromTouch) {
        Date progressDate = recordEntity.getCreationDate();
        c.setTime(progressDate);
        c.add(Calendar.MILLISECOND,progress);
        DateFormat sdf = SimpleDateFormat.getDateTimeInstance();

        progressTimer.setText(DateExtension.milliSecondsToTimer(progress));

        if(localisations.getValue() == null){
            addressBar.setText(disabledGPSInformation+"\n" + sdf.format(c.getTime()));
        }
        else {
            for (RecordLocalisationEntity loca :
                    localisations.getValue()) {
                if (progress >= loca.getTime()) {
                    if (loca.getAddress() == null) {
                        loca.setAddress(disabledGPSInformation);
                    }
                    addressBar.setText(loca.getAddress() + "\n" + sdf.format(c.getTime()));
                }
            }
        }
    }



    @Override
    public void onStartTrackingTouch(SeekBar seekbar) {
        mHandler.removeCallbacks(updateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekbar) {
        mHandler.removeCallbacks(updateTimeTask);
        videoView.seekTo(seekBarVideo.getProgress());
        videoView.start();
        updateProgressBar();
    }

    @OnClick(R.id.play_control)
    public void onControlClick(){
        if(this.videoView.isPlaying()){
            this.videoView.pause();
            this.playControl.setImageResource(android.R.drawable.ic_media_play);
        }
        else{
            this.videoView.start();
            this.playControl.setImageResource(android.R.drawable.ic_media_pause);
            new Handler().postDelayed(() -> {
                this.videoView.setBackground(null);
            },250);
        }
    }

}
