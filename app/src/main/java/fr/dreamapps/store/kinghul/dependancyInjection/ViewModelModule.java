package fr.dreamapps.store.kinghul.dependancyInjection;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.AutoRecordPreferenceViewModel;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.AutoRemovePreferenceViewModel;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.MyRecordsViewModel;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.RecordViewModel;
import fr.dreamapps.store.kinghul.viewModels.AppViewModelFactory;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RecordViewModel.class)
    abstract ViewModel bindRecordViewModel(RecordViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AutoRemovePreferenceViewModel.class)
    abstract ViewModel bindAutoRemovePreferenceViewModel(AutoRemovePreferenceViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AutoRecordPreferenceViewModel.class)
    abstract ViewModel bindAutoRecordPreferenceViewModel(AutoRecordPreferenceViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MyRecordsViewModel.class)
    abstract ViewModel bindMyRecordsViewModel(MyRecordsViewModel repoViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(AppViewModelFactory factory);
}
