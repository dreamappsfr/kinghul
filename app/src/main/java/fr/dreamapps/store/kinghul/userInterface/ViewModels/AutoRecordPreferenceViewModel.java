package fr.dreamapps.store.kinghul.userInterface.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import javax.inject.Inject;

import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;

/**
 * Created by jdeveaux on 21/09/2017.
 */

public class AutoRecordPreferenceViewModel extends AndroidViewModel {
    public final LiveData<Boolean> preferenceEnabled;
    public final LiveData<Boolean> isPremium;
    IPreferencesRepository preferencesRepository;


    @Inject
    public AutoRecordPreferenceViewModel(Application application,  IPreferencesRepository preferencesRepository) {
        super(application);
        this.isPremium = preferencesRepository.isPremium();
        this.preferencesRepository = preferencesRepository;
        this.preferenceEnabled = this.preferencesRepository.getAutoRecord();
    }


    public void setPreferenceAutoRecord(boolean b) {
        this.preferencesRepository.setAutoRecord(b);
    }

}
