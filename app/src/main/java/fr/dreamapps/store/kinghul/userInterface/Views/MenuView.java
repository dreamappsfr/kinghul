package fr.dreamapps.store.kinghul.userInterface.Views;

import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.ButtonMenuItem;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.MenuItem;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.SwitchMenuItem;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.SwitchMenuWithCallToAction;

/**
 * Created by nicolascywier on 27/08/2017.
 */

public class MenuView extends RelativeLayout implements LifecycleObserver {

    @BindView(R.id.menu_image)
    ImageView menuImage;
    @BindView(R.id.menu_text)
    TextView menuText;
    @BindView(R.id.menu_button)
    Button menuButton;
    @BindView(R.id.menu_switch)
    SwitchCompat menuSwitch;

    @BindString(R.string.minutes)
    String minutes;

    @BindDrawable(R.drawable.ic_chevron_right_black_24dp)
    Drawable chevron;
    private IPreferencesRepository preferencesRepository;

    public MenuView(Context context, IPreferencesRepository preferencesRepository) {
        super(context);
        this.preferencesRepository = preferencesRepository;
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.drawer_list_item, this, true);
        ButterKnife.bind(this, itemView);
    }

    public void configure(MenuItem menuItem, LifecycleOwner lifecycleOwner) {
        menuText.setText(menuItem.getText());


        menuImage.setImageResource(menuItem.getImage());

        if (menuItem instanceof SwitchMenuItem) {
            menuSwitch.setVisibility(VISIBLE);
            menuButton.setVisibility(GONE);
            SwitchMenuItem switchMenuItem = (SwitchMenuItem) menuItem;
            switchMenuItem.isChecked().observe(lifecycleOwner, s ->{
                menuSwitch.setChecked(s);
            });
            if(switchMenuItem.getCheckedChangeListener() != null) {
                menuSwitch.setOnTouchListener(switchMenuItem.getCheckedChangeListener());
            }
            this.setOnClickListener(switchMenuItem.getClickListener());
        }
        else if( menuItem instanceof ButtonMenuItem) {
            menuSwitch.setVisibility(GONE);
            menuButton.setVisibility(VISIBLE);
            menuButton.setOnClickListener((menuItem.getCallback()));
            preferencesRepository.getVideoDuration().observe(lifecycleOwner, s -> {
                Long duration = (s / 60000);
                menuButton.setText(minutes.replace("%minutes", duration.toString()));
            });
        }
        else{
            menuSwitch.setVisibility(GONE);
            menuButton.setVisibility(GONE);
            this.setOnClickListener(menuItem.getCallback());
        }

        if((menuItem instanceof SwitchMenuWithCallToAction)){
            menuText.setCompoundDrawablesWithIntrinsicBounds(null,null,chevron,null);
        }
        else {
            menuText.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
        }

    }
}
