package fr.dreamapps.store.kinghul.dataAccess.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.DAO.IDao;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by jdeveaux on 31/07/2017.
 */

@Dao
public abstract class RecordDao implements IDao<RecordEntity>{

    @Insert(onConflict = REPLACE)
    public abstract long save(RecordEntity record);

    @Update
    public abstract void update(RecordEntity recordEntity);

    @Delete
    public abstract void delete(RecordEntity recordEntity);

    @Query("SELECT * FROM recordEntity WHERE id = :id")
    public abstract LiveData<RecordEntity> load(int id);

    @Query("SELECT * FROM recordEntity")
    public abstract LiveData<List<RecordEntity>> load();

    @Query("DELETE FROM recordEntity WHERE saved = 0 AND id NOT IN ( " +
            "  SELECT id FROM recordentity  \n" +
            "  WHERE saved = 0 \n" +
            "  ORDER BY id DESC\n" +
            "  LIMIT :limit  )"
    )
    public abstract void cleanVideosOnTheGo(int limit);

    @Query("SELECT * FROM recordEntity WHERE saved = 0 AND id NOT IN ( " +
            "  SELECT id FROM recordentity  \n" +
            "  WHERE saved = 0 \n" +
            "  ORDER BY id DESC\n" +
            "  LIMIT :limit  )"
    )
    public abstract List<RecordEntity> retrieveVideosToBeRemoved(int limit);

}
