package fr.dreamapps.store.kinghul.userInterface.Fragments;


import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.dependancyInjection.Injectable;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import fr.dreamapps.store.kinghul.userInterface.Adapters.RecordAdapter;
import fr.dreamapps.store.kinghul.userInterface.Adapters.ViewHolders.RecordViewHolder;
import fr.dreamapps.store.kinghul.userInterface.Controllers.NavigationController;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.MyRecordsViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyRecordsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyRecordsFragment extends Fragment implements Injectable {

    @Inject
    IPreferencesRepository preferencesRepository;

    @Inject
    IRecordRepository recordRepository;

    @Inject
    ViewModelProvider.Factory appViewModelFactory;

    @Inject
    NavigationController navigationController;

    @BindView(R.id.record_list)
    RecyclerView recordList;

    @BindView(R.id.empty_view_text)
    TextView emptyText;

    @BindString(R.string.myrecords_remove_popup)
    String removeVideo;

    RecordAdapter recordAdapter;

    MyRecordsViewModel recordViewModel;

    BottomSheetMenuDialog bottomSheet;

    @BindView(R.id.my_records_coordinator_layout)
    public CoordinatorLayout coordinatorLayout;

    public MyRecordsFragment() {
        // Required empty public constructor
    }

    public static MyRecordsFragment newInstance() {
        MyRecordsFragment fragment = new MyRecordsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_recordings, container, false);
        ButterKnife.bind(this,view);

        ((MainActivity) getActivity()).appBarLayoutVisibility(View.VISIBLE);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        supportActionBar.setTitle(R.string.my_records);
        Drawable d = ContextCompat.getDrawable(getActivity(),R.color.colorPrimary);
        supportActionBar.setBackgroundDrawable(d);
        recordViewModel = ViewModelProviders.of(this, appViewModelFactory).get(MyRecordsViewModel.class);

        recordAdapter = new RecordAdapter(recordViewModel, this::displayPopup, parameter -> navigationController.navigateToPlayRecord(parameter));
        recordViewModel.getRecords().observe(this, recordEntities -> {
            recordAdapter.addAll(recordEntities);
            if(recordEntities.isEmpty()){
                emptyText.setVisibility(View.VISIBLE);
            }
            else{
                emptyText.setVisibility(View.GONE);
            }
        });

        recordList.setAdapter(recordAdapter);
        recordList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        return view;
    }

    private void displayPopup(RecordViewHolder recordVH) {
        RecordEntity recordEntity = recordAdapter.getItemAt(recordVH.getAdapterPosition());
        bottomSheet = new BottomSheetBuilder(getActivity(), coordinatorLayout)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.menu_my_records)
                .createDialog();
        bottomSheet.setBottomSheetItemClickListener(item -> {
            switch(item.getItemId()){
                case R.id.menu_share:
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("video/mp4");

                    File fileToShare = new File(recordEntity.getPath());
                    Uri uri = Uri.fromFile(fileToShare);
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(sharingIntent, "Share record"));
                    break;
                case R.id.menu_delete:
                    String[] split = recordEntity.getPath().split("/");
                    String filename = split[split.length-1];
                    String fileNameWithoutExtension = filename.split("\\.")[0];
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(removeVideo.replace("%filename", fileNameWithoutExtension));
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Non",
                            (dialog, which) -> dialog.dismiss());
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Oui",
                            (dialog, which) ->
                                    recordViewModel.deleteRecord(recordEntity));
                    alertDialog.show();
                    int color = getResources().getColor(R.color.colorPrimary);
                    alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(color);
                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(color);

                    break;

                case R.id.menu_rename:
                    recordVH.setEditableMode();
                    break;
                default:
                    break;
            }
            bottomSheet.dismiss();
        });
        bottomSheet.show();
    }

}
