package fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories;

import android.arch.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.Abstract.IAbstractRepository;

/**
 * Created by jdeveaux on 31/07/2017.
 */

public interface IRecordRepository extends IAbstractRepository<RecordEntity> {
    void insert(String path, boolean shouldSaveVideo, ArrayList<RecordLocalisationEntity> recordLocalisationEntities);
    //void insertLocalisationEntitiesFor(RecordEntity entity);
    void update(RecordEntity entity);
    void delete(RecordEntity entity);
    LiveData<List<RecordLocalisationEntity>> retrieveLocalisationFor(RecordEntity recordEntity);
}
