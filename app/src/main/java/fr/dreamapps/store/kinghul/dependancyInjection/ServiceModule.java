package fr.dreamapps.store.kinghul.dependancyInjection;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import fr.dreamapps.store.kinghul.service.RecordingService;

/**
 * Created by jdeveaux on 13/09/2017.
 */

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    abstract RecordingService bindRecordingService();
}
