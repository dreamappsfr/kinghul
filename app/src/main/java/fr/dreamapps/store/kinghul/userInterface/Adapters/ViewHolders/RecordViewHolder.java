package fr.dreamapps.store.kinghul.userInterface.Adapters.ViewHolders;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.text.DateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface.ICallback;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface.ICallbackWithParam;

/**
 * Created by jdeveaux on 25/09/2017.
 */

public class RecordViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.record_name)
    TextView recordName;

    @BindView(R.id.record_time)
    TextView recordTime;

    @BindView(R.id.record_save_button)
    ImageView saveButton;

    @BindView(R.id.record_play_button)
    ImageView playButton;

    @BindView(R.id.more_actions)
    ImageView moreActionButton;

    @BindView(R.id.my_switcher)
    ViewSwitcher switcher;

    @BindView(R.id.hidden_edit_view)
    EditText editText;

    int colorPrimary;

    Context context;
    private RecordEntity record;
    private ICallbackWithParam<RecordEntity, String> renameCallback;

    public RecordViewHolder(View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        colorPrimary = ResourcesCompat.getColor(context.getResources(), R.color.colorPrimary, null);
        this.context = context;

    }

    public void configure(RecordEntity record, ICallback<RecordViewHolder> displayPopupCallback, ICallback<RecordEntity> saveCallback, ICallback<RecordEntity> playCallback, ICallbackWithParam<RecordEntity, String> renameCallback) {
        this.record = record;
        this.renameCallback = renameCallback;
        String[] split = record.getPath().split("/");
        String filename = split[split.length-1];
        String fileNameWithoutExtension = filename.split("\\.")[0];
        Date date = record.getCreationDate();
        DateFormat df = DateFormat.getDateTimeInstance();
        recordName.setText(fileNameWithoutExtension);
        if(date != null) {
            recordTime.setText(df.format(date));
        }

        if(!record.isSaved()){
            saveButton.setImageResource(R.drawable.ic_star_border_black_24dp);
            saveButton.setColorFilter(Color.BLACK);
        }
        else{
            saveButton.setImageResource(R.drawable.ic_star_black_24dp);
            saveButton.setColorFilter(colorPrimary);
        }



        itemView.setOnLongClickListener(view -> {
            displayPopupCallback.invoke(this);
            return true;
        });
        moreActionButton.setOnClickListener(view ->{
            displayPopupCallback.invoke(this);
        });

        saveButton.setOnClickListener(view -> {
            saveCallback.invoke(record);
        });
        this.itemView.setOnClickListener(view -> {
            playCallback.invoke(record);
        });
    }

    private boolean editTextValidation(RecordEntity record, ICallbackWithParam<RecordEntity, String> renameCallback) {
        editText.setOnFocusChangeListener(null);
        switcher.showPrevious();
        String newName = editText.getText().toString();
        if(newName.isEmpty()){
            return true;
        }
        recordName.setText(newName);
        String oldPath = record.getPath();
        String[] splitNew = oldPath.split("/");
        if (newName.split("\\.").length == 1) {
            String[] splitFileExtension = splitNew[splitNew.length - 1].split("\\.");
            newName += "."+splitFileExtension[splitFileExtension.length - 1];
        }
        splitNew[splitNew.length - 1] = newName;

        record.setPath(TextUtils.join("/", splitNew));
        renameCallback.invoke(record, oldPath);
        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        return false;
    }

    public void setEditableMode() {
        switcher.showNext();
        editText.setText(recordName.getText());


        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            editText.requestFocus();
            editText.selectAll();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            editText.setOnEditorActionListener((textView, actionId, keyEvent) -> {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    editTextValidation(record, renameCallback);
                    return true;
                }
                return false;
            });

            editText.setOnFocusChangeListener((view1, b) -> {
                editTextValidation(record, renameCallback);
            });
        }, 100);

    }
}
