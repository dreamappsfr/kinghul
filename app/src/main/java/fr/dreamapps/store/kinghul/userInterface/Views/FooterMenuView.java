package fr.dreamapps.store.kinghul.userInterface.Views;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;

/**
 * Created by nicolascywier on 28/08/2017.
 */

public class FooterMenuView extends RelativeLayout {

    private Context context;
    private boolean isPremium;

    public FooterMenuView(Context context, boolean isPremium) {
        super(context);
        this.isPremium = isPremium;
        this.context = context;
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @LayoutRes int layout = isPremium ? R.layout.footer_item_menu_premium:R.layout.footer_item_menu;
        View itemView = inflater.inflate(layout, this, true);

        ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.footer_button)
    public void unlockFeatures() {
        if(isPremium) return;

        ((MainActivity)context).onBuyPremiumClick();
    }

}
