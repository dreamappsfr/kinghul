package fr.dreamapps.store.kinghul.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.lifecycle.LifecycleService;
import android.arch.lifecycle.MutableLiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.SurfaceTexture;
import android.location.Address;
import android.location.Geocoder;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface.IRecordingInitialization;
import fr.dreamapps.store.kinghul.service.threads.RenderThread;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.RecordViewModel;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;
import static android.content.Intent.CATEGORY_LAUNCHER;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.location.GpsStatus.GPS_EVENT_STARTED;
import static android.location.GpsStatus.GPS_EVENT_STOPPED;

public class RecordingService extends LifecycleService {

    public static final String ACTION_RETURN_TO_ACTIVITY = "ACTION_RETURN_TO_ACTIVITY";
    public static final String ACTION_TOGGLE_RECORD = "ACTION_TOGGLE_RECORD";
    public static final String ACTION_TOGGLE_SOUND = "ACTION_TOGGLE_SOUND";
    public static final String ACTION_TOGGLE_SAVE = "ACTION_TOGGLE_SAVE";
    public static final String ACTION_KILL_SERVICE = "ACTION_KILL_SERVICE";
    public static final String ACTION_FINISH_ACTIVITY = "ACTION_FINISH_ACTIVITY";
    public static final String NO_ACTION = "NO_ACTION";
    private static final int REQUEST_CODE = 341;
    private static final String TAG = "Camera2VideoFragment";

    // This is the object that receives interactions from clients.
    private final IBinder binder = new RecordingService.RecordBinder();
    @Inject
    IRecordRepository recordRepository;
    @Inject
    IPreferencesRepository preferencesRepository;
    private Notification notif;
    private Handler timerHandler;
    private Runnable timerRunnable;
    private RemoteViews contentView;
    private RecordViewModel recordViewModel;
    private boolean shouldSaveVideo = false;
    private ArrayList<RecordLocalisationEntity> savedLocalisation;
    private MutableLiveData<Boolean> isRecording;
    private MutableLiveData<String> addressLiveData;


    private Geocoder geocoder;
    private FusedLocationProviderClient locationProviderClient;
    private Task<LocationSettingsResponse> locationSettingsResponseTask;
    private long startingTime;
    private LocationCallback locationCallback;
    private boolean mRequestingLocationUpdates;
    private RenderThread mRenderThread;
    private SettingsClient settingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationManager locationManager;


    public RecordingService() {
    }

    public MutableLiveData<String> getAddressLiveData() {
        return addressLiveData;
    }

    public void setCurrentCamera() {
        Log.d(TAG,"setCurrentCamera");
        if(this.getIsRecordingVideo().getValue() ){
            Toast.makeText(this, "You cannot switch camera while recording", Toast.LENGTH_SHORT).show();
            return;
        }

        this.mRenderThread.getHandler().sendSwitchCamera();
    }


    public void toggleSound() {
        Log.d(TAG,"toggleSound");
        if(this.getIsRecordingVideo().getValue()){
            Toast.makeText(this, "You cannot enable or disable sound while recording", Toast.LENGTH_SHORT).show();
            return;
        }
        recordViewModel.toggleSound();
        AudioManager myAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        myAudioManager.setMode(AudioManager.MODE_NORMAL);
        if(myAudioManager.isMicrophoneMute() != recordViewModel.getIsSoundEnable().getValue()) {
            myAudioManager.setMicrophoneMute(recordViewModel.getIsSoundEnable().getValue());
        }
    }

    public void launchNotif() {
        Log.d(TAG,"launchNotif");
        Intent toggleRecordingIntent = new Intent(getApplicationContext(), RecordingService.class);
        toggleRecordingIntent.setAction(ACTION_TOGGLE_RECORD);
        PendingIntent piToggleRecording = PendingIntent.getService(getApplicationContext(), REQUEST_CODE, toggleRecordingIntent, 0);

        Intent toggleSaveCurrentRecordIntent = new Intent(getApplicationContext(), RecordingService.class);
        toggleSaveCurrentRecordIntent.setAction(ACTION_TOGGLE_SAVE);
        PendingIntent piToggleSaveCurrentRecordIntent = PendingIntent.getService(getApplicationContext(), REQUEST_CODE, toggleSaveCurrentRecordIntent, 0);

        contentView = new RemoteViews(getPackageName(),R.layout.notification_layout);
        contentView.setOnClickPendingIntent(R.id.toggle_recording,piToggleRecording);
        contentView.setOnClickPendingIntent(R.id.toggle_save_current_record,piToggleSaveCurrentRecordIntent);

        notif = getNotificationBuilder()
                .build();
        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(01, notif);
    }

    public NotificationCompat.Builder getNotificationBuilder() {
        Log.d(TAG,"getNotificationBuilder");
        Intent returnToActivityIntent = new Intent(getApplicationContext(), RecordingService.class);
        returnToActivityIntent.setAction(ACTION_RETURN_TO_ACTIVITY);
        PendingIntent piReturnToActivity = PendingIntent.getService(getApplicationContext(), REQUEST_CODE, returnToActivityIntent, 0);

        Intent deleteActivityIntent = new Intent(getApplicationContext(), RecordingService.class);
        deleteActivityIntent.setAction(ACTION_FINISH_ACTIVITY);
        PendingIntent piDeleteActivity = PendingIntent.getService(getApplicationContext(), REQUEST_CODE, deleteActivityIntent, 0);

        return new NotificationCompat.Builder(this, DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContent(contentView)
                .setPriority(Notification.PRIORITY_MAX)
                .setCustomBigContentView(contentView)
                .setContentIntent(piReturnToActivity)
                .setDeleteIntent(piDeleteActivity)
                .setUsesChronometer(true);
    }

    private BroadcastReceiver mGpsSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED"))
            {
                // react on GPS provider change action
                Log.d(TAG,"GPS Switch State");
                toggleLocation();
            }
        }
    };

    public void toggleLocation() {
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            initLocationTask();
        }
        else{
            locationSettingsResponseTask = null;
            if(locationCallback != null) {
                locationProviderClient.removeLocationUpdates(locationCallback);
            }
            addressLiveData.setValue(getApplication().getResources().getString(R.string.disabled_gps));
        }
    }

    public MutableLiveData<Boolean> getIsRecordingVideo() {
        return isRecording;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        Log.d(TAG,"onCreate");
        super.onCreate();

        addressLiveData = new MutableLiveData<>();
        addressLiveData.setValue(getApplication().getResources().getString(R.string.disabled_gps));

        isRecording = new MutableLiveData<>();
        isRecording.setValue(false);
        geocoder = new Geocoder(getApplication(), Locale.getDefault());
        locationManager = (LocationManager) getApplication().getSystemService(Context.LOCATION_SERVICE);

        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        settingsClient = LocationServices.getSettingsClient(this);

        toggleLocation();
        // this will help some phones to trigger the next event handler
        registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    @SuppressLint("MissingPermission")
    private void initLocationTask() {
        Log.d(TAG,"initLocationTask");
        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
        startLocationUpdates();
    }

    private LocationRequest createLocationRequest() {
        Log.d(TAG,"createLocationRequest");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Log.d(TAG, "onLocationResult");
                for (Location location : locationResult.getLocations()) {
                    try {
                        Address addressObject = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0);
                        String numeroRue = addressObject.getSubThoroughfare();
                        numeroRue = (numeroRue == null) ? "" : numeroRue;
                        String address = numeroRue + " " + addressObject.getThoroughfare() + ", \n" + addressObject.getPostalCode() + " " + addressObject.getLocality();
                        addressLiveData.setValue(address);
                        if (isRecording.getValue()) {
                            RecordLocalisationEntity entity = new RecordLocalisationEntity();
                            entity.setAddress(address);
                            entity.setTime(SystemClock.elapsedRealtime() - startingTime);
                            savedLocalisation.add(entity);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }
    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {

        mRequestingLocationUpdates = true;
        // Begin by checking if the device has the necessary location settings.
        settingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(locationSettingsResponse -> {
                    Log.i(TAG, "All location settings are satisfied.");

                    //noinspection MissingPermission
                    locationProviderClient.requestLocationUpdates(mLocationRequest,
                            locationCallback, Looper.myLooper());
                })
                .addOnFailureListener(e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    Log.e(TAG, "StatusCode : "+statusCode);

                    mRequestingLocationUpdates = false;
                });
    }

    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        locationProviderClient.removeLocationUpdates(locationCallback)
                .addOnCompleteListener(task -> mRequestingLocationUpdates = false);
    }

        @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"onBind");
        super.onBind(intent);
        AndroidInjection.inject(this);
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG,"onStartCommand");
        super.onStartCommand(intent,flags,startId);
        if(intent == null || intent.getAction() == null) return START_STICKY;
        switch (intent.getAction()){
            case ACTION_RETURN_TO_ACTIVITY:
                Intent goToActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                goToActivityIntent.setAction(Intent.ACTION_MAIN);
                goToActivityIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                goToActivityIntent.addCategory(CATEGORY_LAUNCHER);
                startActivity(goToActivityIntent);

                break;
            case ACTION_TOGGLE_RECORD:
                this.toggleRecording();
                Toast.makeText(this, R.string.video_saved, Toast.LENGTH_SHORT).show();
                break;
            case ACTION_TOGGLE_SAVE:
                this.toggleSaveRecord();
                break;
            case ACTION_TOGGLE_SOUND:
                this.recordViewModel.toggleSound();
                this.toggleSound();
                break;
            case ACTION_KILL_SERVICE:
                if(!isRecording.getValue()) {
                    this.onUnbind(intent);
                }
                break;
            case ACTION_FINISH_ACTIVITY:
                Intent deleteActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                deleteActivityIntent.setAction(ACTION_FINISH_ACTIVITY);
                deleteActivityIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(deleteActivityIntent);
                break;
            case NO_ACTION:
            default:
                break;
        }
        return START_STICKY;
    }

    private void toggleSaveRecord() {
        Log.d(TAG,"toggleSaveRecord");
        shouldSaveVideo = !shouldSaveVideo;
        if(shouldSaveVideo){
            Toast.makeText(this, R.string.video_starred, Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, R.string.video_not_starred, Toast.LENGTH_SHORT).show();
        }
        this.updateNotification();
    }


    public void initialize( RecordViewModel recordViewModel, IRecordingInitialization recordingInitialization){
        Log.d(TAG,"initialize");
        timerHandler = new Handler();
        savedLocalisation = new ArrayList<>();
        this.recordViewModel = recordViewModel;
        this.launchNotif();
        this.startForeground(01,notif);
        if(mRenderThread == null) {
            mRenderThread = new RenderThread(recordViewModel, this, recordRepository);
            mRenderThread.setName("TexFromCam Render");
            mRenderThread.start();
            mRenderThread.waitUntilReady();
        }
        if (this.recordViewModel.getAutoRecordingValue() && recordingInitialization != null) {
            recordingInitialization.onComplete();
        }

    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG,"onUnbind");
        this.stopForeground(true);
        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(01);
        if(locationCallback != null) {
            locationProviderClient.removeLocationUpdates(locationCallback);
        }

        try {
            unregisterReceiver(mGpsSwitchStateReceiver);
        }
        catch(Exception e){
            Log.e(TAG,"Unregister not registered receiver");
        }
        return true;
    }

    public void toggleRecording() {
        Log.d(TAG,"toggleRecording");
        if (this.getIsRecordingVideo().getValue()) {
            stopRecordingVideo();
        } else {
            startRecordingVideo();
        }
        updateNotification();
    }

    public long getStartingTime() {
        Log.d(TAG,"getStartingTime");
        return startingTime;
    }

    private void updateNotification() {
        Log.d(TAG,"updateNotification");
        String notificationButton = this.getIsRecordingVideo().getValue()? getResources().getString(R.string.stop):getResources().getString(R.string.start);
        contentView.setTextViewText(R.id.toggle_recording,notificationButton);

        int starResourceId = shouldSaveVideo? R.drawable.ic_star_black_24dp:R.drawable.ic_star_border_black_24dp;
        contentView.setImageViewResource(R.id.toggle_save_current_record, starResourceId);

        String notificationTitle = this.getIsRecordingVideo().getValue()? getResources().getString(R.string.notification_recording):getResources().getString(R.string.notification_stopped);
        contentView.setTextViewText(R.id.title,notificationTitle);

        contentView.setViewVisibility(R.id.touch_here, this.getIsRecordingVideo().getValue()?View.GONE:View.VISIBLE);

        String duration = new SimpleDateFormat("mm:ss").format(new Date(preferencesRepository.getVideoDuration().getValue()));
        contentView.setChronometer(R.id.chronometer,this.getStartingTime(),"%s / "+duration,true);

        if(this.getIsRecordingVideo().getValue()){
            this.contentView.setViewVisibility(R.id.toggle_save_current_record, View.VISIBLE);
            this.contentView.setViewVisibility(R.id.toggle_recording, View.VISIBLE);
            this.contentView.setViewVisibility(R.id.chronometer, View.VISIBLE);
        }
        else{
            this.contentView.setViewVisibility(R.id.toggle_save_current_record, View.GONE);
            this.contentView.setViewVisibility(R.id.toggle_recording, View.GONE);
            this.contentView.setViewVisibility(R.id.chronometer, View.GONE);
        }

        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        try {
            notificationManager.notify(01, notif);
        }
        catch (RuntimeException e){
            Toast.makeText(this, "notification has encoutered an issue", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "notify did not work properly", e);
        }
    }

    private void startRecordingVideo() {
        Log.d(TAG, "startRecordingVideo");

        this.savedLocalisation.clear();
        RecordLocalisationEntity entity = new RecordLocalisationEntity();
        entity.setAddress(this.addressLiveData.getValue());
        entity.setTime(0);
        this.savedLocalisation.add(entity);
        startingTime = SystemClock.elapsedRealtime();
        this.isRecording.setValue(true);
        updateNotification();
        timerRunnable = () -> {
            Long elapsedMillis = SystemClock.elapsedRealtime() - this.getStartingTime();
            if (elapsedMillis > recordViewModel.getVideoDuration().getValue()) {
                toggleRecording();
            }
            else {
                timerHandler.postDelayed(timerRunnable, 2000);
            }
        };
        timerHandler.post(timerRunnable);
        mRenderThread.getHandler().startRecord();
    }

    private void stopRecordingVideo() {
        Log.d(TAG,"stoprecordingvideo");
        this.timerHandler.removeCallbacks(timerRunnable);
        mRenderThread.getHandler().stopRecord(shouldSaveVideo, this.savedLocalisation);

        this.shouldSaveVideo = false;
        this.isRecording.setValue(false);

        Long elapsedMillis = SystemClock.elapsedRealtime() - startingTime;
        boolean doesVideoReachMaxDuration = false;
        if (preferencesRepository.getVideoDuration().getValue() != null && elapsedMillis > preferencesRepository.getVideoDuration().getValue()) {
            doesVideoReachMaxDuration = true;
        }
        startingTime = SystemClock.elapsedRealtime();
        this.stopForeground(false);
        this.updateNotification();

        if(preferencesRepository.getShouldRestartRecording().getValue() && doesVideoReachMaxDuration){
            new Handler().postDelayed(
                    this::toggleRecording, 1000
            );
        }
    }

    public void surfaceCreated(SurfaceHolder holder, boolean newSurface) {
        mRenderThread.getHandler().sendSurfaceAvailable(holder, newSurface);
    }

    public void surfaceChanged(int format, int width, int height) {
        mRenderThread.getHandler().sendSurfaceChanged(format, width, height);
    }

    public void surfaceDestroyed() {
        mRenderThread.getHandler().sendSurfaceDestroyed();
    }

    public void reinitCamera() {
        mRenderThread.getHandler().sendReloadCamera();
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class RecordBinder extends Binder {
        public RecordingService getService() {
            return RecordingService.this;
        }
    }


}
