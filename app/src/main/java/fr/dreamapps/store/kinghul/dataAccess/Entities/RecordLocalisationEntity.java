package fr.dreamapps.store.kinghul.dataAccess.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

import java.util.Date;

import fr.dreamapps.store.kinghul.dataAccess.Entities.Abstract.AbstractEntity;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jdeveaux on 28/07/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = RecordEntity.class,
        parentColumns = "id",
        childColumns = "record_id",
        onUpdate = CASCADE,
        onDelete = CASCADE))
public class RecordLocalisationEntity extends AbstractEntity  {
    public RecordLocalisationEntity() {
        this.setCreationDate(new Date());
    }

    @ColumnInfo(name="record_id")
    private long recordEntityId;
    private String address;
    private long time;


    public long getRecordEntityId() {
        return recordEntityId;
    }

    public void setRecordEntityId(long recordEntity) {
        this.recordEntityId = recordEntity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }


}
