package fr.dreamapps.store.kinghul.domainObjects.Interfaces.DAO;

import android.arch.lifecycle.LiveData;

import java.util.List;

import fr.dreamapps.store.kinghul.dataAccess.Entities.Abstract.AbstractEntity;

/**
 * Created by jdeveaux on 02/08/2017.
 */

public interface IDao<T extends AbstractEntity> {
    long save(T entity);

    LiveData<T> load(int entityId);
    LiveData<List<T>> load();
}
