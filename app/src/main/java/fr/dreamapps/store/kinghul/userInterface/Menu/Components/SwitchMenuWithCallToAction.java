package fr.dreamapps.store.kinghul.userInterface.Menu.Components;

import android.arch.lifecycle.LiveData;
import android.view.View;
import android.widget.CompoundButton;

/**
 * Created by jdeveaux on 07/10/2017.
 */

public class SwitchMenuWithCallToAction extends SwitchMenuItem {
    public SwitchMenuWithCallToAction(int image, String text, boolean isPremium, LiveData<Boolean> switchValue, View.OnTouchListener checkedChangeListener, View.OnClickListener clickListener) {
        super(image, text, switchValue, isPremium, checkedChangeListener, clickListener);
    }
}
