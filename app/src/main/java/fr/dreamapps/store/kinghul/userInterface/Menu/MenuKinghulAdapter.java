package fr.dreamapps.store.kinghul.userInterface.Menu;

import android.arch.lifecycle.LifecycleOwner;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.MenuItem;
import fr.dreamapps.store.kinghul.userInterface.Views.CategoryMenuView;
import fr.dreamapps.store.kinghul.userInterface.Views.FooterMenuView;
import fr.dreamapps.store.kinghul.userInterface.Views.MenuView;

/**
 * Created by nicolascywier on 27/08/2017.
 */

public class MenuKinghulAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<MenuItem> menuItemList;
    private IPreferencesRepository preferencesRepository;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private LifecycleOwner lifecycleOwner;

    public static final int SECTION_OTHER_POSITION = 7;

    public MenuKinghulAdapter(List<MenuItem> menuItemList, IPreferencesRepository preferencesRepository, LifecycleOwner lifecycleOwner) {
        this.menuItemList = menuItemList;
        this.preferencesRepository = preferencesRepository;
        this.lifecycleOwner = lifecycleOwner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            return new MenuViewHolder(new MenuView(parent.getContext(), preferencesRepository));
        } else if (viewType == TYPE_HEADER) {
            return new CategoryMenuViewHolder(new CategoryMenuView(parent.getContext()));
        } else if (viewType == TYPE_FOOTER) {
            return new FooterMenuViewHolder(new FooterMenuView(parent.getContext(), preferencesRepository.isPremium().getValue()));
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure you're using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MenuViewHolder) {
            MenuViewHolder menuViewHolder = (MenuViewHolder) holder;
            MenuItem item;
            if(position<2){
                item =  menuItemList.get(0);
            }
            else{
                item = menuItemList.get(position<SECTION_OTHER_POSITION && position > 2? position-2 : position-3);
            }
            menuViewHolder.menuItemView.configure(item, lifecycleOwner);
        } else if (holder instanceof CategoryMenuViewHolder) {
            CategoryMenuViewHolder categoryMenuViewHolder = (CategoryMenuViewHolder) holder;
            categoryMenuViewHolder.categoryMenuView.configure(position);
        } else if (holder instanceof FooterMenuViewHolder) {
            // Do nothing
        }
    }

    @Override
    public int getItemCount() {
        return menuItemList.size()+3;
    }

    private static class MenuViewHolder extends RecyclerView.ViewHolder {

        MenuView menuItemView;

        MenuViewHolder(View itemView) {
            super(itemView);
            menuItemView = (MenuView) itemView;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 2 || position == SECTION_OTHER_POSITION) {
            return TYPE_HEADER;
        }
        else if (position == 0) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private static class CategoryMenuViewHolder extends RecyclerView.ViewHolder {

        CategoryMenuView categoryMenuView;

        CategoryMenuViewHolder(View itemView) {
            super(itemView);
            categoryMenuView = (CategoryMenuView) itemView;
        }
    }

    private static class FooterMenuViewHolder extends RecyclerView.ViewHolder {

        FooterMenuView footerMenuView;

        FooterMenuViewHolder(View itemView) {
            super(itemView);
            footerMenuView = (FooterMenuView) itemView;
        }
    }
}
