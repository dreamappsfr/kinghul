package fr.dreamapps.store.kinghul.userInterface.ViewModels;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import fr.dreamapps.store.kinghul.userInterface.Fragments.HomeFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.RecordFragment;

import static android.location.GpsStatus.GPS_EVENT_STARTED;
import static android.location.GpsStatus.GPS_EVENT_STOPPED;

/**
 * Created by jdeveaux on 12/08/2017.
 */

public class RecordViewModel extends AndroidViewModel {

    private MutableLiveData<String> date;

    private LiveData<Boolean> isSoundEnable;

    private LiveData<Long> videoDuration;
    private IPreferencesRepository preferencesRepository;

    public boolean hasFrontCamera;
    private LiveData<Integer> currentCamera;


    private final BroadcastReceiver m_timeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                    action.equals(Intent.ACTION_TIMEZONE_CHANGED) ||
                    action.equals(Intent.ACTION_TIME_TICK)) {
                setDateValue();
            }
        }
    };


    @SuppressLint("MissingPermission")
    @Inject
    public RecordViewModel(Application application, IPreferencesRepository preferencesRepository) {
        super(application);
        this.preferencesRepository = preferencesRepository;
        this.videoDuration = preferencesRepository.getVideoDuration();
        this.isSoundEnable = preferencesRepository.getEnableSound();

        date = new MutableLiveData<>();
        currentCamera = preferencesRepository.getPreferedCamera();

        setDateValue();
        IntentFilter s_intentFilter;
        s_intentFilter = new IntentFilter();
        s_intentFilter.addAction(Intent.ACTION_TIME_TICK);
        s_intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        s_intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        application.registerReceiver(m_timeChangedReceiver, s_intentFilter);

        hasFrontCamera = false;

    }






    private void setDateValue(){
        date.setValue(DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT,getApplication().getResources().getConfiguration().locale).format(new Date()));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        getApplication().unregisterReceiver(m_timeChangedReceiver);
    }

    public LiveData<String> getDate(){
        return date;
    }
    public LiveData<Long> getVideoDuration() {
        return videoDuration;
    }
    public LiveData<Boolean> getIsSoundEnable(){
        return isSoundEnable;
    }
    public LiveData<Integer> getCurrentCamera() {
        return currentCamera;
    }

    public void toggleCurrentCamera() {
        if(Camera.getNumberOfCameras()<2) return;
        if(currentCamera.getValue() == Camera.CameraInfo.CAMERA_FACING_BACK){
            this.preferencesRepository.setPreferedCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        }
        else {
            this.preferencesRepository.setPreferedCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
        }

    }

    public void toggleSound() {
        if(isSoundEnable.getValue()) {
            preferencesRepository.setEnableSound(false);
        }
        else{
            preferencesRepository.setEnableSound(true);
        }
    }

    public Boolean getAutoRecordingValue() {
        return preferencesRepository.getAutoRecord().getValue();
    }
}
