package fr.dreamapps.store.kinghul.dataAccess.Repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import fr.dreamapps.store.kinghul.dataAccess.DAO.RecordDao;
import fr.dreamapps.store.kinghul.dataAccess.DAO.RecordLocalisationDao;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.dataAccess.Repositories.Abstract.AbstractRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;

/**
 * Created by jdeveaux on 31/07/2017.
 */

public class RecordRepository extends AbstractRepository<RecordEntity, RecordDao> implements IRecordRepository {
    private Application application;
    private IPreferencesRepository preferencesRepository;
    private RecordLocalisationDao recordLocalisationDao;
    @Inject
    public RecordRepository(RecordDao dao, RecordLocalisationDao recordLocalisationDao, IPreferencesRepository preferencesRepository, Application application) {
        this.recordLocalisationDao = recordLocalisationDao;
        this.dao = dao;
        this.preferencesRepository = preferencesRepository;
        this.application = application;
    }

    @Override
    public void insert(final String path, boolean shouldSaveVideo, final ArrayList<RecordLocalisationEntity> recordLocalisationEntities) {
                new InsertAsync(path, shouldSaveVideo).execute((ArrayList<RecordLocalisationEntity>)recordLocalisationEntities.clone());
    }

    private class InsertAsync extends AsyncTask<List<RecordLocalisationEntity>, Void,Void> {

        private String path;
        private boolean shouldSaveVideo;

        public InsertAsync(String path, boolean shouldSaveVideo) {
            this.path = path;

            this.shouldSaveVideo = shouldSaveVideo;
        }

        @Override
        protected Void doInBackground(List<RecordLocalisationEntity>... params) {
            RecordEntity entity = new RecordEntity();
            entity.setPath(path);
            entity.setSaved(shouldSaveVideo);
            long id = dao.save(entity);
            for (RecordLocalisationEntity r :
                    params[0]) {
                r.setRecordEntityId(id);
                recordLocalisationDao.save(r);
            }
            if(preferencesRepository.getShouldAutomaticallyRemoveRecording().getValue()) {
                checkVideosToRemove();
            }
            return null;
        }
    }

    @Override
    public void update(RecordEntity entity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                dao.update(entity);
                return null;
            }
        }.execute();
    }

    @Override
    public void delete(RecordEntity entity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                File f = new File(entity.getPath());
                f.delete();
                dao.delete(entity);
                return null;
            }
        }.execute();
    }



    private void checkVideosToRemove(){
        int limit = preferencesRepository.getVideoQuantityBeforeDeleting().getValue();
        List<RecordEntity> toBeRemoved = this.dao.retrieveVideosToBeRemoved(limit);

        for (RecordEntity record :
                toBeRemoved) {
            File file = new File(record.getPath());
            file.delete();
            application.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        }

        this.dao.cleanVideosOnTheGo(limit);
    }

    public LiveData<List<RecordLocalisationEntity>> retrieveLocalisationFor(RecordEntity recordEntity){
        return this.recordLocalisationDao.loadFromRecordId(((int) recordEntity.getId()));
    }

}
