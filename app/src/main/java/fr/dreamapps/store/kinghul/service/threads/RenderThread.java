package fr.dreamapps.store.kinghul.service.threads;

/**
 * Created by jdeveaux on 07/03/2018.
 */

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.dataAccess.Repositories.RecordRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;
import fr.dreamapps.store.kinghul.service.RecordingService;
import fr.dreamapps.store.kinghul.service.gles.CameraUtils;
import fr.dreamapps.store.kinghul.service.gles.Drawable2d;
import fr.dreamapps.store.kinghul.service.gles.EglCore;
import fr.dreamapps.store.kinghul.service.gles.GlUtil;
import fr.dreamapps.store.kinghul.service.gles.ScaledDrawable2d;
import fr.dreamapps.store.kinghul.service.gles.Sprite2d;
import fr.dreamapps.store.kinghul.service.gles.Texture2dProgram;
import fr.dreamapps.store.kinghul.service.gles.WindowSurface;
import fr.dreamapps.store.kinghul.userInterface.Fragments.RecordFragment;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.RecordViewModel;

/**
 * Thread that handles all rendering and camera operations.
 */
public class RenderThread extends Thread implements
        SurfaceTexture.OnFrameAvailableListener {
    private MediaRecorder mMediaRecorder;
    private final Context mContext;
    private final IRecordRepository mRecordRepository;


    // Object must be created on render thread to get correct Looper, but is used from
    // UI thread, so we need to declare it volatile to ensure the UI thread sees a fully
    // constructed object.
    private volatile RenderHandler mHandler;

    // Requested values; actual may differ.
    private static final int REQ_CAMERA_WIDTH = 1920;
    private static final int REQ_CAMERA_HEIGHT = 1080;
    private static final int REQ_CAMERA_FPS = 30;

    private String TAG = "RenderThread";

    // Used to wait for the thread to start.
    private Object mStartLock = new Object();
    private boolean mReady = false;


    private Camera mCamera;
    private int mCameraPreviewWidth, mCameraPreviewHeight;

    private EglCore mEglCore;
    private WindowSurface mWindowSurface;
    private int mWindowSurfaceWidth;
    private int mWindowSurfaceHeight;

    // Receives the output from the camera preview.
    private SurfaceTexture mCameraTexture;

    // Orthographic projection matrix.
    private float[] mDisplayProjectionMatrix = new float[16];

    private Texture2dProgram mTexProgram;
    private final ScaledDrawable2d mRectDrawable =
            new ScaledDrawable2d(Drawable2d.Prefab.RECTANGLE);
    private final Sprite2d mRect = new Sprite2d(mRectDrawable);
    private RecordViewModel mRecordVM;

    private int mRotationDegree;
    private float mPosX;
    private float mPosY;
    private String nextVideoFilePath;
    private int mCameraFacing;


    /**
     * Constructor.  Pass in the MainHandler, which allows us to send stuff back to the
     * Activity.
     */
    public RenderThread( RecordViewModel recordVM, Context context, IRecordRepository recordRepository) {
        mRecordVM = recordVM;
        mContext = context;
        mRecordRepository = recordRepository;
    }

    /**
     * Thread entry point.
     */
    @Override
    public void run() {
        Looper.prepare();

        // We need to create the Handler before reporting ready.
        mHandler = new RenderHandler(this);
        synchronized (mStartLock) {
            mReady = true;
            mStartLock.notify();    // signal waitUntilReady()
        }

        // Prepare EGL and open the camera before we start handling messages.
        mEglCore = new EglCore(null, 0);
        openCamera(REQ_CAMERA_WIDTH, REQ_CAMERA_HEIGHT, REQ_CAMERA_FPS);

        Looper.loop();

        Log.d(TAG, "looper quit");
        releaseCamera();
        releaseGl();
        mEglCore.release();

        synchronized (mStartLock) {
            mReady = false;
        }
    }


    private void reloadCamera() {
        synchronized (mStartLock) {
            mReady = true;
            mStartLock.notify();    // signal waitUntilReady()
        }
        releaseCamera();
        releaseGl();
        mEglCore.release();

        mEglCore = new EglCore(null, 0);
        openCamera(REQ_CAMERA_WIDTH, REQ_CAMERA_HEIGHT, REQ_CAMERA_FPS);
        synchronized (mStartLock) {
            mReady = false;
        }
    }

    /**
     * Waits until the render thread is ready to receive messages.
     * <p>
     * Call from the UI thread.
     */
    public void waitUntilReady() {
        synchronized (mStartLock) {
            while (!mReady) {
                try {
                    mStartLock.wait();
                } catch (InterruptedException ie) { /* not expected */ }
            }
        }
    }

    /**
     * Shuts everything down.
     */
    private void shutdown() {
        Log.d(TAG, "shutdown");
        Looper.myLooper().quit();
    }

    /**
     * Returns the render thread's Handler.  This may be called from any thread.
     */
    public RenderHandler getHandler() {
        return mHandler;
    }

    /**
     * Handles the surface-created callback from SurfaceView.  Prepares GLES and the Surface.
     */
    private void surfaceAvailable(SurfaceHolder holder, boolean newSurface) {
        Surface surface = holder.getSurface();

        mWindowSurface = new WindowSurface(mEglCore, surface, false);
        mWindowSurface.makeCurrent();

        // Create and configure the SurfaceTexture, which will receive frames from the
        // camera.  We set the textured rect's program to render from it.
        if(mTexProgram == null) {
            mTexProgram = new Texture2dProgram(Texture2dProgram.ProgramType.TEXTURE_EXT);
            int textureId = mTexProgram.createTextureObject();
            mCameraTexture = new SurfaceTexture(textureId);
            mRect.setTexture(textureId);
        }
        if (!newSurface) {
            // This Surface was established on a previous run, so no surfaceChanged()
            // message is forthcoming.  Finish the surface setup now.
            //
            // We could also just call this unconditionally, and perhaps do an unnecessary
            // bit of reallocating if a surface-changed message arrives.
            mWindowSurfaceWidth = mWindowSurface.getWidth();
            mWindowSurfaceHeight = mWindowSurface.getHeight();
            finishSurfaceSetup();
        }

        mCameraTexture.setOnFrameAvailableListener(this);
    }

    /**
     * Releases most of the GL resources we currently hold (anything allocated by
     * surfaceAvailable()).
     * <p>
     * Does not release EglCore.
     */
    private void releaseGl() {
        GlUtil.checkGlError("releaseGl start");

        if (mWindowSurface != null) {
            mWindowSurface.release();
            mWindowSurface = null;
        }
        if (mTexProgram != null) {
            mTexProgram.release();
            mTexProgram = null;
        }
        GlUtil.checkGlError("releaseGl done");

        mEglCore.makeNothingCurrent();
    }

    /**
     * Handles the surfaceChanged message.
     * <p>
     * We always receive surfaceChanged() after surfaceCreated(), but surfaceAvailable()
     * could also be called with a Surface created on a previous run.  So this may not
     * be called.
     */
    private void surfaceChanged(int width, int height) {
        Log.d(TAG, "RenderThread surfaceChanged " + width + "x" + height);

        mWindowSurfaceWidth = width;
        mWindowSurfaceHeight = height;
        finishSurfaceSetup();
    }

    /**
     * Handles the surfaceDestroyed message.
     */
    private void surfaceDestroyed() {
        // In practice this never appears to be called -- the activity is always paused
        // before the surface is destroyed.  In theory it could be called though.
        Log.d(TAG, "RenderThread surfaceDestroyed");
        //releaseGl();
    }

    /**
     * Sets up anything that depends on the window size.
     * <p>
     * Open the camera (to set mCameraAspectRatio) before calling here.
     */
    private void finishSurfaceSetup() {
        int width = mWindowSurfaceWidth;
        int height = mWindowSurfaceHeight;
        Log.d(TAG, "finishSurfaceSetup size=" + width + "x" + height +
                " camera=" + mCameraPreviewWidth + "x" + mCameraPreviewHeight);

        // Use full window.
        GLES20.glViewport(0, 0, width, height);

        // Simple orthographic projection, with (0,0) in lower-left corner.


        // Default position is center of screen.
        mPosX = width / 2.0f;
        mPosY = height / 2.0f;

        WindowManager window = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        int screenOrientation = window.getDefaultDisplay()
                .getRotation();
        int rotation;
        switch (screenOrientation) {
            case Surface.ROTATION_0: rotation = 0; break;
            case Surface.ROTATION_90: rotation = 90; break;
            case Surface.ROTATION_180: rotation = 180; break;
            case Surface.ROTATION_270: rotation = 270; break;
            default: rotation = 0;
        }


        int rotationRect ;
        if(this.mCameraFacing == Camera.CameraInfo.CAMERA_FACING_FRONT){
            Matrix.orthoM(mDisplayProjectionMatrix, 0, width, 0, 0, height, -1, 1);
            rotationRect = (360-rotation+90)%360;

        }
        else{
            Matrix.orthoM(mDisplayProjectionMatrix, 0, 0, width, 0, height, -1, 1);
            rotationRect = (rotation - 90) %360;
        }

        this.setRotate(rotationRect);

        updateGeometry();

        // Ready to go, start the camera.
        Log.d(TAG, "starting camera preview");

        try {
            mCamera.reconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            mCamera.setPreviewTexture(mCameraTexture);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        mCamera.startPreview();
    }



    @Override   // SurfaceTexture.OnFrameAvailableListener; runs on arbitrary thread
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        mHandler.sendFrameAvailable();
    }

    /**
     * Handles incoming frame of data from the camera.
     */
    private void frameAvailable() {
        mCameraTexture.updateTexImage();
        draw();
    }

    private void updateGeometry() {
        int width = mWindowSurfaceWidth;
        int height = mWindowSurfaceHeight;

        int smallDim = Math.min(width, height);
        // Max scale is a bit larger than the screen, so we can show over-size.
        float scaled = smallDim;
        float cameraAspect = (float) mCameraPreviewWidth / mCameraPreviewHeight;
        int newWidth = Math.round(scaled * cameraAspect);
        int newHeight = Math.round(scaled);

        float zoomFactor = 1.0f;
        int rotAngle = mRotationDegree;

        mRect.setScale(newWidth, newHeight);
        mRect.setPosition(mPosX, mPosY);
        mRect.setRotation(rotAngle);
        mRectDrawable.setScale(zoomFactor);

    }

    private void setPosition(int x, int y) {
        mPosX = x;
        mPosY = mWindowSurfaceHeight - y;   // GLES is upside-down
        updateGeometry();
    }
    
    /**
     * Draws the scene and submits the buffer.
     */
    private void draw() {
        GlUtil.checkGlError("draw start");

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        mRect.draw(mTexProgram, mDisplayProjectionMatrix);
        mWindowSurface.swapBuffers();

        GlUtil.checkGlError("draw done");
    }


    /**
     * Opens a camera, and attempts to establish preview mode at the specified width
     * and height with a fixed frame rate.
     * <p>
     * Sets mCameraPreviewWidth / mCameraPreviewHeight.
     */
    private void openCamera(int desiredWidth, int desiredHeight, int desiredFps) {
        if (mCamera != null) {
            throw new RuntimeException("camera already initialized");
        }

        Camera.CameraInfo info = new Camera.CameraInfo();

        // Try to find a front-facing camera (e.g. for videoconferencing).
        Integer cameraId = mRecordVM.getCurrentCamera().getValue();
        try {
            mCamera = Camera.open(cameraId); // attempt to get a Camera instance
            android.hardware.Camera.getCameraInfo(cameraId, info);
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Log.e(TAG, "Camera "+cameraId+" is not available");
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        if (mCamera == null) {
            Log.d(TAG, "No front-facing camera found; opening default");
            mCamera = Camera.open();    // opens first back-facing camera
        }
        if (mCamera == null) {
            throw new RuntimeException("Unable to open camera");
        }

        Camera.Parameters parms = mCamera.getParameters();

        CameraUtils.choosePreviewSize(parms, desiredWidth, desiredHeight);

        // Try to set the frame rate to a constant value.
        int thousandFps = CameraUtils.chooseFixedPreviewFps(parms, desiredFps * 1000);

        this.mCameraFacing = info.facing;

        int rotation = getRotation(info);

        parms.setRotation(rotation);
        mCamera.setDisplayOrientation(rotation);

        mCamera.setParameters(parms);
        int[] fpsRange = new int[2];
        Camera.Size mCameraPreviewSize = parms.getPreviewSize();
        parms.getPreviewFpsRange(fpsRange);
        String previewFacts = mCameraPreviewSize.width + "x" + mCameraPreviewSize.height;
        if (fpsRange[0] == fpsRange[1]) {
            previewFacts += " @" + (fpsRange[0] / 1000.0) + "fps";
        } else {
            previewFacts += " @[" + (fpsRange[0] / 1000.0) +
                    " - " + (fpsRange[1] / 1000.0) + "] fps";
        }
        Log.i(TAG, "Camera config: " + previewFacts);

        mCameraPreviewWidth = mCameraPreviewSize.width;
        mCameraPreviewHeight = mCameraPreviewSize.height;
    }

    public int getRotation(Camera.CameraInfo info) {
        WindowManager window = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        int rotation = window.getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360  - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    /**
     * Stops camera preview, and releases the camera to the system.
     */
    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            Log.d(TAG, "releaseCamera -- done");
        }
    }

    public void setRotate(int rotate) {
        this.mRotationDegree = rotate;
        updateGeometry();
    }

    private void setUpMediaRecorder() throws IOException {
        Log.d(TAG,"setUpMediaRecorder");
        try {
            mCamera.unlock();
        }
        catch (RuntimeException e){
            Toast.makeText(mContext, "Camera is used in another process", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Camera can't be unlocked", e);
        }
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setCamera(mCamera);

        if(mRecordVM.getIsSoundEnable().getValue()) {
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        }

        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);

        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoFrameRate(profile.videoFrameRate);
        mMediaRecorder.setVideoSize(profile.videoFrameWidth, profile.videoFrameHeight);
        mMediaRecorder.setVideoEncodingBitRate(profile.videoBitRate);
        mMediaRecorder.setVideoEncoder(profile.videoCodec);

        if(mRecordVM.getIsSoundEnable().getValue()) {
            mMediaRecorder.setAudioEncodingBitRate(profile.audioBitRate);
            mMediaRecorder.setAudioChannels(profile.audioChannels);
            mMediaRecorder.setAudioSamplingRate(profile.audioSampleRate);
            mMediaRecorder.setAudioEncoder(profile.audioCodec);
        }

        mMediaRecorder.setOutputFile(getNextVideoFilePath(mContext));
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(mRecordVM.getCurrentCamera().getValue(), info);

        WindowManager window = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        int screenOrientation = window.getDefaultDisplay()
                .getRotation();
        int rotation;



        int rotationRect ;
        if(this.mCameraFacing == Camera.CameraInfo.CAMERA_FACING_FRONT){
            switch (screenOrientation) {
                case Surface.ROTATION_0: rotation = 360; break;
                case Surface.ROTATION_90: rotation = 90; break;
                case Surface.ROTATION_180: rotation = 180; break;
                case Surface.ROTATION_270: rotation = 270; break;
                default: rotation = 0;
            }
            rotationRect = (rotation-90)%360;

        }
        else{
            switch (screenOrientation) {
                case Surface.ROTATION_0: rotation = 360; break;
                case Surface.ROTATION_90: rotation = 270; break;
                case Surface.ROTATION_180: rotation = 180; break;
                case Surface.ROTATION_270: rotation = 90; break;
                default: rotation = 0;
            }
            rotationRect = (rotation + 90) %360;
        }

        mMediaRecorder.setOrientationHint(rotationRect);

        mMediaRecorder.setOnErrorListener((mediaRecorder, i, i1) -> {
            Log.d(TAG, "mMedia Recorder on Error : "+ i);
        });

        mMediaRecorder.setOnInfoListener((mediaRecorder, i, i1) -> {
            Log.d(TAG, "mMedia Recorder on Info : "+ i);
        });

        mMediaRecorder.prepare();
    }

    public String getNextVideoFilePath(Context context){
        this.nextVideoFilePath = getVideoFilePath(context);
        return this.nextVideoFilePath;
    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + System.currentTimeMillis() + ".mp4";
    }

    private void startRecording() {
        try {
            setUpMediaRecorder();
            mMediaRecorder.start();
        } catch (IOException e) {
            Log.e(TAG,e.getMessage());
        }
    }


    private void stopRecording(boolean shouldSave, ArrayList<RecordLocalisationEntity> localisations) {
        mRecordRepository.insert(this.nextVideoFilePath, shouldSave,localisations);
        this.nextVideoFilePath = null;
        try {
            mMediaRecorder.stop();
            mMediaRecorder.reset();
        }
        catch(RuntimeException e ){
            Log.e(TAG,e.getMessage());
        }
    }


    private void switchCamera() {
        this.releaseCamera();
        this.openCamera(REQ_CAMERA_WIDTH, REQ_CAMERA_HEIGHT, REQ_CAMERA_FPS);
    }

    /**
     * Handler for RenderThread.  Used for messages sent from the UI thread to the render thread.
     * <p>
     * The object is created on the render thread, and the various "send" methods are called
     * from the UI thread.
     */
    public static class RenderHandler extends Handler {
        private static final int MSG_SURFACE_AVAILABLE = 0;
        private static final int MSG_SURFACE_CHANGED = 1;
        private static final int MSG_SURFACE_DESTROYED = 2;
        private static final int MSG_SHUTDOWN = 3;
        private static final int MSG_FRAME_AVAILABLE = 4;
        private static final int MSG_ZOOM_VALUE = 5;
        private static final int MSG_SIZE_VALUE = 6;
        private static final int MSG_ROTATE_VALUE = 7;
        private static final int MSG_POSITION = 8;
        private static final int MSG_REDRAW = 9;
        private static final int MSG_START_RECORDING = 10;
        private static final int MSG_STOP_RECORDING = 11;
        private static final int MSG_SWITCH_CAMERA = 12;
        private static final int MSG_RELOAD_CAMERA = 13;

        // This shouldn't need to be a weak ref, since we'll go away when the Looper quits,
        // but no real harm in it.
        private WeakReference<RenderThread> mWeakRenderThread;

        /**
         * Call from render thread.
         */
        RenderHandler(RenderThread rt) {
            mWeakRenderThread = new WeakReference<RenderThread>(rt);
        }

        /**
         * Sends the "surface available" message.  If the surface was newly created (i.e.
         * this is called from surfaceCreated()), set newSurface to true.  If this is
         * being called during Activity startup for a previously-existing surface, set
         * newSurface to false.
         * <p>
         * The flag tells the caller whether or not it can expect a surfaceChanged() to
         * arrive very soon.
         * <p>
         * Call from UI thread.
         */
        public void sendSurfaceAvailable(SurfaceHolder holder, boolean newSurface) {
            sendMessage(obtainMessage(MSG_SURFACE_AVAILABLE,
                    newSurface ? 1 : 0, 0, holder));
        }

        /**
         * Sends the "surface changed" message, forwarding what we got from the SurfaceHolder.
         * <p>
         * Call from UI thread.
         */
        public void sendSurfaceChanged(@SuppressWarnings("unused") int format, int width,
                                       int height) {
            // ignore format
            sendMessage(obtainMessage(MSG_SURFACE_CHANGED, width, height));
        }

        /**
         * Sends the "shutdown" message, which tells the render thread to halt.
         * <p>
         * Call from UI thread.
         */
        public void sendSurfaceDestroyed() {
            sendMessage(obtainMessage(MSG_SURFACE_DESTROYED));
        }

        /**
         * Sends the "shutdown" message, which tells the render thread to halt.
         * <p>
         * Call from UI thread.
         */
        public void sendShutdown() {
            sendMessage(obtainMessage(MSG_SHUTDOWN));
        }

        /**
         * Sends the "record" message.
         */
        public void startRecord(){ sendMessage(obtainMessage(MSG_START_RECORDING));}

        /**
         * Sends the "abort recording" message.
         */
        public void stopRecord(boolean shouldSave, ArrayList<RecordLocalisationEntity> localisationEntities){
            Message msg = obtainMessage(MSG_STOP_RECORDING);
            Bundle b = new Bundle();
            b.putBoolean("ShouldSave", shouldSave);
            b.putSerializable("Localisations", localisationEntities);
            msg.setData(b);
            sendMessage(msg);
        }

        /**
         * Sends the "frame available" message.
         * <p>
         * Call from UI thread.
         */
        public void sendFrameAvailable() {
            sendMessage(obtainMessage(MSG_FRAME_AVAILABLE));
        }

        /**
         * Sends the "zoom value" message.  "progress" should be 0-100.
         * <p>
         * Call from UI thread.
         */
        public void sendZoomValue(int progress) {
            sendMessage(obtainMessage(MSG_ZOOM_VALUE, progress, 0));
        }

        /**
         * Sends the "size value" message.  "progress" should be 0-100.
         * <p>
         * Call from UI thread.
         */
        public void sendSizeValue(int progress) {
            sendMessage(obtainMessage(MSG_SIZE_VALUE, progress, 0));
        }

        /**
         * Sends the "rotate value" message.  "progress" should be 0-100.
         * <p>
         * Call from UI thread.
         */
        public void sendRotateValue(int rotation) {
            sendMessage(obtainMessage(MSG_ROTATE_VALUE, rotation, 0));
        }

        /**
         * Sends the "position" message.  Sets the position of the rect.
         * <p>
         * Call from UI thread.
         */
        public void sendPosition(int x, int y) {
            sendMessage(obtainMessage(MSG_POSITION, x, y));
        }

        /**
         * Sends the "redraw" message.  Forces an immediate redraw.
         * <p>
         * Call from UI thread.
         */
        public void sendRedraw() {
            sendMessage(obtainMessage(MSG_REDRAW));
        }

        public void sendSwitchCamera() { sendMessage(obtainMessage(MSG_SWITCH_CAMERA));}

        public void sendReloadCamera() { sendMessage(obtainMessage(MSG_RELOAD_CAMERA));}


        @Override  // runs on RenderThread
        public void handleMessage(Message msg) {
            int what = msg.what;
            Log.d("RenderHandler", "RenderHandler [" + this + "]: what=" + what);

            RenderThread renderThread = mWeakRenderThread.get();
            if (renderThread == null) {
                return;
            }

            switch (what) {
                case MSG_SURFACE_AVAILABLE:
                    renderThread.surfaceAvailable((SurfaceHolder) msg.obj, msg.arg1 != 0);
                    break;
                case MSG_SURFACE_CHANGED:
                    renderThread.surfaceChanged(msg.arg1, msg.arg2);
                    break;
                case MSG_SURFACE_DESTROYED:
                    renderThread.surfaceDestroyed();
                    break;
                case MSG_SHUTDOWN:
                    renderThread.shutdown();
                    break;
                case MSG_FRAME_AVAILABLE:
                    renderThread.frameAvailable();
                    break;
                case MSG_REDRAW:
                    renderThread.draw();
                    break;
                case MSG_ROTATE_VALUE:
                    renderThread.setRotate(msg.arg1);
                    break;
                case MSG_START_RECORDING:
                    renderThread.startRecording();
                    break;
                case MSG_STOP_RECORDING:
                    renderThread.stopRecording(msg.getData().getBoolean("ShouldSave"), (ArrayList<RecordLocalisationEntity>) msg.getData().getSerializable("Localisations"));
                    break;
                case MSG_SWITCH_CAMERA:
                    renderThread.switchCamera();
                    break;
                case MSG_RELOAD_CAMERA:
                    renderThread.reloadCamera();
                    break;
                default:
                    throw new RuntimeException("unknown message " + what);
            }
        }
    }

}


