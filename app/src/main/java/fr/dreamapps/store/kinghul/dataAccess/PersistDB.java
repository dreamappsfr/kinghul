package fr.dreamapps.store.kinghul.dataAccess;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import fr.dreamapps.store.kinghul.dataAccess.DAO.RecordDao;
import fr.dreamapps.store.kinghul.dataAccess.DAO.RecordLocalisationDao;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;

/**
 * Created by jdeveaux on 31/07/2017.
 */


@Database(entities = {RecordEntity.class, RecordLocalisationEntity.class}, version = 1)
public abstract class PersistDB extends RoomDatabase {
    public abstract RecordDao recordDao();
    public abstract RecordLocalisationDao recordLocalisationDao();
}