package fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface;

/**
 * Created by jdeveaux on 27/09/2017.
 */

public interface ICallback<T> {
    void invoke(T parameter);
}

