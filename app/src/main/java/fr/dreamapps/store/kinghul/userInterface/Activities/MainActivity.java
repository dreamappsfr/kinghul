package fr.dreamapps.store.kinghul.userInterface.Activities;

import android.app.PendingIntent;
import android.arch.lifecycle.LifecycleRegistry;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.service.RecordingService;
import fr.dreamapps.store.kinghul.userInterface.Controllers.NavigationController;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.MenuItem;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.ButtonMenuItem;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.SwitchMenuWithCallToAction;
import fr.dreamapps.store.kinghul.userInterface.Menu.MenuKinghulAdapter;
import fr.dreamapps.store.kinghul.userInterface.Menu.Components.SwitchMenuItem;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static fr.dreamapps.store.kinghul.userInterface.Fragments.HomeFragment.PERMISSIONS;
import static fr.dreamapps.store.kinghul.userInterface.Fragments.HomeFragment.REQUEST_PERMISSIONS_FRAGMENT;

public class MainActivity extends AppCompatActivity implements
        HasSupportFragmentInjector {
    public static final int REQUEST_PERMISSIONS_ACTIVITY = 1013;
    // Binds
    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;
    public RecordingService recordingService;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bar_layout)
    AppBarLayout mBarLayout;
    @BindView(R.id.left_drawer)
    RecyclerView mDrawerList;

    @BindString(R.string.home)
    String home;

    @BindString(R.string.record_duration)
    String recordDuration;

    @BindString(R.string.infinite_recording)
    String infiniteRecording;

    @BindString(R.string.auto_record)
    String autoRecord;

    @BindString(R.string.auto_remove)
    String autoRemove;

    @BindString(R.string.my_records)
    String myRecords;

    @BindString(R.string.suggestions)
    String Suggestions;

    @BindString(R.string.share_app)
    String shareApp;

    @BindString(R.string.send_logs)
    String send_log;

    @BindString(R.string.sharing_text)
    String sharingText;

    private ActionBarDrawerToggle drawerToggle;

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    @Inject
    NavigationController navigationController;
    @Inject
    IPreferencesRepository preferencesRepository;

    IInAppBillingService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            Bundle ownedItems;
            try {
                ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
                int response = ownedItems.getInt("RESPONSE_CODE");
                if (response == 0) {
                    ArrayList<String> ownedSkus =
                            ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                    ArrayList<String>  purchaseDataList =
                            ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                    ArrayList<String>  signatureList =
                            ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");

                    for (int i = 0; i < purchaseDataList.size(); ++i) {
                        String sku = ownedSkus.get(i);
                        if(sku.equals("upgrade_premium")){
                            unlockFeatures();
                        }

                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }
    };


    public ActionBarDrawerToggle getDrawerToggle() {
        return this.drawerToggle;
    }

    public void appBarLayoutVisibility(int visibility) {
        this.mBarLayout.setVisibility(visibility);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent.getAction() != null && intent.getAction().equals(RecordingService.ACTION_FINISH_ACTIVITY)){
            this.recordingService.stopSelf();
            this.finish();
        }

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        startService(serviceIntent);
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);



        setSupportActionBar(toolbar);

        Window window = getWindow();


        this.drawerToggle = new ActionBarDrawerToggle(this,this.drawerLayout, toolbar,0,0);
        this.drawerLayout.addDrawerListener(this.drawerToggle);


        // Init the menu list
        List<MenuItem> menuItemList = new ArrayList<>();
        menuItemList.add(new MenuItem(R.drawable.ic_home_black_24dp, home, false, view -> {
            navigationController.navigateToHome(true);
            this.drawerLayout.closeDrawers();
        }));
        menuItemList.add(new ButtonMenuItem(R.drawable.ic_av_timer_black_24dp, recordDuration, true,(View view) -> {
            if(!preferencesRepository.isPremium().getValue()){
                Toast.makeText(this, R.string.premium_feature, Toast.LENGTH_SHORT).show();
                return;
            }
            if(navigationController.isServiceUp() && recordingService != null && recordingService.getIsRecordingVideo().getValue()){
                Toast.makeText(this, R.string.disabled_on_play, Toast.LENGTH_SHORT).show();
                return;
            }
            final int flags = window.getDecorView().getSystemUiVisibility();
            final AlertDialog.Builder builder = new AlertDialog.Builder(this)

                    .setView(R.layout.dialog_picker);

            View titleView= getLayoutInflater().inflate(R.layout.title_bar, null);
            TextView titleTV = titleView.findViewById(R.id.title_dialog);
            titleTV.setText(R.string.record_duration);
            builder.setCustomTitle(titleView);
            AlertDialog d = builder.create();

            d.show();

            final Button button = d.findViewById(R.id.done);
            final NumberPicker np = d.findViewById(R.id.numberPicker1);
            np.setMinValue(1);
            np.setMaxValue(4);
            np.setValue(1);
            np.setDisplayedValues( new String[] { "3", "5", "10", "15" } );
            np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            np.setWrapSelectorWheel(false);
            np.setOnValueChangedListener((numberPicker, i, i1) -> {
                long finalNumber = 180000;
                switch (i1) {
                    case 1:
                        finalNumber = 180000;
                        break;
                    case 2:
                        finalNumber = 300000;
                        break;
                    case 3:
                        finalNumber = 600000;
                        break;
                    case 4 :
                        finalNumber = 900000;
                        break;
                }
                preferencesRepository.setVideoDuration(finalNumber);
            });
            button.setOnClickListener(view1 -> {
                window.getDecorView().setSystemUiVisibility(flags);
                if(np.getValue() == 1) {
                    preferencesRepository.setVideoDuration(180000);
                }
                d.dismiss();
            });
            np.setOnClickListener(view1 -> {
                window.getDecorView().setSystemUiVisibility(flags);
                d.dismiss();
            });
        }));
        menuItemList.add(new SwitchMenuItem(R.drawable.ic_repeat_black_24dp,infiniteRecording, preferencesRepository.getShouldRestartRecording(), false, (view, m) -> {
            if(m.getAction() == MotionEvent.ACTION_DOWN) {
                boolean willSwitchBeToggledOn = !((SwitchCompat) view).isChecked();
                if (willSwitchBeToggledOn != preferencesRepository.getShouldRestartRecording().getValue()) {
                    preferencesRepository.setShouldRestartRecording(willSwitchBeToggledOn);
                }
                return false;
            }
            return true;
        }, null));
        menuItemList.add(new SwitchMenuWithCallToAction(R.drawable.ic_videocam_black_24dp, autoRecord,true, preferencesRepository.getAutoRecord(),  (view,m) -> {
            if(m.getAction() == MotionEvent.ACTION_DOWN) {
                if (!preferencesRepository.isPremium().getValue()) {
                    Toast.makeText(this, R.string.premium_feature, Toast.LENGTH_SHORT).show();
                    return true;
                }
                boolean willSwitchBeToggledOn = !((SwitchCompat) view).isChecked();
                if (willSwitchBeToggledOn != preferencesRepository.getAutoRecord().getValue()) {
                    if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(this, RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this,
                                PERMISSIONS,
                                REQUEST_PERMISSIONS_ACTIVITY);
                    }
                    preferencesRepository.setAutoRecord(willSwitchBeToggledOn);
                }
                return false;
            }
            return true;
        }, view -> {

            navigationController.navigateToAutoRecordPreferences();
            this.drawerLayout.closeDrawers();
        }));
        menuItemList.add(new SwitchMenuWithCallToAction(R.drawable.ic_delete_forever_black_24dp, autoRemove,true, preferencesRepository.getShouldAutomaticallyRemoveRecording(), (view,m) -> {
            if(m.getAction() == MotionEvent.ACTION_DOWN) {
                if (!preferencesRepository.isPremium().getValue()) {
                    Toast.makeText(this, R.string.premium_feature, Toast.LENGTH_SHORT).show();
                    return true;
                }
                boolean willSwitchBeToggledOn = !((SwitchCompat) view).isChecked();
                if (willSwitchBeToggledOn != preferencesRepository.getShouldAutomaticallyRemoveRecording().getValue()) {
                    preferencesRepository.setShouldAutomaticallyRemoveRecording(willSwitchBeToggledOn);
                }
                return false;
            }
            else{
                return true;
            }
        }, view -> {

            navigationController.navigateToAutoRemovePreferences();
            this.drawerLayout.closeDrawers();
        }));
        menuItemList.add(new MenuItem(R.drawable.ic_play_circle_outline_black_24dp, myRecords, false, view -> {
            navigationController.navigateToMyRecordsPreferences();
            this.drawerLayout.closeDrawers();
        }));
        menuItemList.add(new MenuItem(R.drawable.ic_comment_black_24dp,  Suggestions,false, view -> {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto","contact@kinghul.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "SUGGESTIONS");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }));
        menuItemList.add(new MenuItem(R.drawable.ic_share_black_24dp,  shareApp,false, view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,  sharingText.replace("%appUrl","https://play.google.com/apps/testing/fr.dreamapps.store.kinghul"));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }));

        // Set the adapter for the list view
        mDrawerList.setLayoutManager(new LinearLayoutManager(this));
        mDrawerList.setAdapter(new MenuKinghulAdapter(menuItemList, preferencesRepository, this));

        if(preferencesRepository.getAutoRecord().getValue()){
            new Handler().postDelayed(() -> {
                navigationController.navigateToHome(false);
                new Handler().postDelayed(() -> {
                    navigationController.navigateToRecord();
                }, 500);
            }, 100);

        }
        else if (savedInstanceState == null) {
            navigationController.navigateToSplash();
        }

        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        super.onBackPressed();
        this.navigationController.onBackPressed();
    }

    public void resetAccount(){
        this.preferencesRepository.setPremiumEnabled(false);
    }

    public void onBuyPremiumClick(){
        try {
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(),
                    "upgrade_premium", "inapp", "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
            if (pendingIntent != null) {
                startIntentSenderForResult(pendingIntent.getIntentSender(),
                        1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                        Integer.valueOf(0));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001 && data != null) {
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    if(sku.equals("upgrade_premium")) {
                        unlockFeatures();
                    }
                }
                catch (JSONException e) {
                }
            }
        }
    }





    private void unlockFeatures() {
        preferencesRepository.setPremiumEnabled(true);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_PERMISSIONS_FRAGMENT || requestCode == REQUEST_PERMISSIONS_ACTIVITY){
            for (int result :
                    grantResults) {
                if(result == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this,"Sorry!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

        if(requestCode == REQUEST_PERMISSIONS_ACTIVITY){

        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @NonNull
    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }


    // This callback is called only when there is a saved instance previously saved using
// onSaveInstanceState(). We restore some state in onCreate() while we can optionally restore
// other state here, possibly usable after onStart() has completed.
// The savedInstanceState Bundle is same as the one used in onCreate().
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        mService = IInAppBillingService.Stub.asInterface(savedInstanceState.getBinder("BILLING_SERVICE"));
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mService != null) {
            outState.putBinder("BILLING_SERVICE", mService.asBinder());
        }
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

}
