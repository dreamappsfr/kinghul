package fr.dreamapps.store.kinghul.userInterface.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface.ICallback;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface.ICallbackWithParam;
import fr.dreamapps.store.kinghul.userInterface.Adapters.ViewHolders.RecordViewHolder;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.MyRecordsViewModel;

/**
 * Created by jdeveaux on 25/09/2017.
 */

public class RecordAdapter extends RecyclerView.Adapter<RecordViewHolder> {

    private List<RecordEntity> recordList;
    private MyRecordsViewModel recordViewModel;
    private ICallback<RecordViewHolder> displayPopupCallback;
    private ICallback<RecordEntity> playCallback;
    private Context context;

    public RecordAdapter(MyRecordsViewModel recordViewModel, ICallback<RecordViewHolder> displayPopupCallback, ICallback<RecordEntity> playCallback) {
        this.recordViewModel = recordViewModel;
        this.displayPopupCallback = displayPopupCallback;
        this.playCallback = playCallback;
        recordList = new ArrayList<>();
    }

    @Override
    public RecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        context = parent.getContext();
        View cell = inflater.inflate(R.layout.row_record, parent, false);
        return new RecordViewHolder(cell, parent.getContext());
    }

    @Override
    public void onBindViewHolder(RecordViewHolder holder, int position) {
        holder.configure(this.recordList.get(position),displayPopupCallback, new SaveCallback(), playCallback, new RenameCallback());
    }


    @Override
    public int getItemCount() {
        return recordList.size();
    }

    public void addAll(List<RecordEntity> rt){
        recordList.clear();
        recordList.addAll(rt);
        Collections.sort(recordList, (lhs, rhs) -> {
            if(rhs.getCreationDate() != null && lhs.getCreationDate() != null) {
                return rhs.getCreationDate().compareTo(lhs.getCreationDate());
            }
            return 0;
        });
        notifyDataSetChanged();
    }

    public RecordEntity getItemAt(int adapterPosition) {
        return recordList.get(adapterPosition);
    }

    private class SaveCallback implements ICallback<RecordEntity>{
        @Override
        public void invoke(RecordEntity recordEntity) {

            recordViewModel.setSaveFor(recordEntity, !recordEntity.isSaved());

        }
    }

    private class RenameCallback implements ICallbackWithParam<RecordEntity, String> {
        @Override
        public void invoke(RecordEntity recordEntity, String oldPath) {
            recordViewModel.updatePathFor(recordEntity, oldPath);
        }
    }
}
