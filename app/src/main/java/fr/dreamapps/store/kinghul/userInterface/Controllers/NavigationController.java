package fr.dreamapps.store.kinghul.userInterface.Controllers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


import javax.inject.Inject;

import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.service.RecordingService;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import fr.dreamapps.store.kinghul.userInterface.Fragments.AutoRecordPreferenceFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.AutoRemovePreferenceFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.HomeFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.MyRecordsFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.PlayRecordFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.RecordFragment;
import fr.dreamapps.store.kinghul.userInterface.Fragments.SplashFragment;

/**
 * Created by jdeveaux on 02/08/2017.
 */

public class NavigationController {
    private final int containerId;
    private final FragmentManager fragmentManager;
    private final Context context;
    private static boolean serviceUp = false;
    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
        this.context = mainActivity.getBaseContext();
    }

    public void onBackPressed() {
        int index = fragmentManager.getBackStackEntryCount() - 1;

        while (index >= 0){
            FragmentManager.BackStackEntry backEntry = fragmentManager.getBackStackEntryAt(index);
            String tag = backEntry.getName();
            if (tag.equals("home") || tag.equals("record") || tag.equals("MyRecordsFragment")) {
                break;
            }
            fragmentManager.popBackStack();
            index--;
        }
    }



    public void navigateToSplash() {
        SplashFragment splashFragment = SplashFragment.newInstance();
        fragmentManager.beginTransaction()
                .replace(containerId, splashFragment)
                .commitAllowingStateLoss();
    }

    public void splashDismiss() {
        this.navigateToHome(false);
    }

    public void navigateToHome(boolean killService){
        HomeFragment fragment = HomeFragment.newInstance();
        String tag = "home";

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment, tag);
        transaction.commitAllowingStateLoss();

        if(killService) shouldKillService();
    }


    public void navigateToRecord(){
        RecordFragment fragment = RecordFragment.newInstance();
        String tag = "record";
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();

        shouldKillService();
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startMain);
    }

    public void navigateToAutoRemovePreferences() {
        AutoRemovePreferenceFragment fragment = AutoRemovePreferenceFragment.newInstance();
        String tag = "AutoRemovePreference";

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.addToBackStack(tag);
        transaction.replace(containerId, fragment, tag);
        transaction.commitAllowingStateLoss();

        shouldKillService();
    }

    public void navigateToAutoRecordPreferences() {
        AutoRecordPreferenceFragment fragment = AutoRecordPreferenceFragment.newInstance();
        String tag = "AutoRecordPreference";

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.addToBackStack(tag);
        transaction.replace(containerId, fragment, tag);
        transaction.commitAllowingStateLoss();

        shouldKillService();
    }

    public void navigateToMyRecordsPreferences() {
        MyRecordsFragment fragment = MyRecordsFragment.newInstance();
        String tag = "MyRecordsFragment";

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();

        shouldKillService();
    }

    public void navigateToPlayRecord(RecordEntity parameter) {
        PlayRecordFragment fragment = PlayRecordFragment.newInstance(parameter);
        String tag = "playRecord"+parameter.getId();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();

        shouldKillService();
    }

    private void shouldKillService(){
        if(this.isServiceUp()) {
            Intent kill = new Intent(context, RecordingService.class);
            kill.setAction(RecordingService.ACTION_KILL_SERVICE);
            context.startService(kill);
            serviceUp = false;
        }
    }

    public void setServiceUp(boolean isUp) {
        serviceUp = isUp;
    }

    public boolean isServiceUp() {
        return serviceUp;
    }
}
