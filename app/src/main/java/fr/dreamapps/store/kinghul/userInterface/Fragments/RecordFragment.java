package fr.dreamapps.store.kinghul.userInterface.Fragments;


import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Repositories.RecordRepository;
import fr.dreamapps.store.kinghul.dependancyInjection.Injectable;
import fr.dreamapps.store.kinghul.service.RecordingService;
import fr.dreamapps.store.kinghul.service.threads.RenderThread;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import fr.dreamapps.store.kinghul.userInterface.Controllers.NavigationController;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.RecordViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RecordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecordFragment extends Fragment implements LifecycleRegistryOwner, Injectable, SurfaceHolder.Callback {

    private final String TAG = "RecordFragment";


    @BindView(R.id.cameraView)
    public SurfaceView CameraPreviewContainer;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    @Inject
    ViewModelProvider.Factory appViewModelFactory;
    @BindView(R.id.record)
    ImageView recordButton;
    @BindView(R.id.mute)
    ImageView muteButton;
    @BindView(R.id.switch_camera)
    ImageView switchCamera;
    @BindView(R.id.minimize_app)
    ImageView minimizeButton;
    @BindView(R.id.video_max_duration)
    TextView videoMaxDuration;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.date_time)
    TextView dateTime;
    @BindView(R.id.chrono)
    Chronometer chronometer;
    @BindView(R.id.burger_menu)
    ImageView burgerMenu;
    @BindView(R.id.record_fragment)
    View view;
    @Inject
    NavigationController navigationController;
    @Inject
    RecordRepository recordRepository;
    private RecordViewModel recordViewModel;
    private Intent intent;
    private RecordingService recordingService;
    private LiveData<Boolean> isServiceRecording;
    private boolean shouldUnbindService = true;

    private static SurfaceHolder sSurfaceHolder;

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {

            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            navigationController.setServiceUp(true);
            recordingService = ((RecordingService.RecordBinder)service).getService();
            ((MainActivity) getActivity()).recordingService = recordingService;

            recordingService.initialize(RecordFragment.this.recordViewModel, () -> {
                shouldUnbindService = false;
                new Handler().postDelayed(()-> {
                    onClickMinimize();
                    recordingService.toggleRecording();
                },1500);
            });
            if(sSurfaceHolder != null) {
                recordingService.surfaceCreated(sSurfaceHolder, true);
                recordingService.surfaceChanged(RecordFragment.this.format, RecordFragment.this.width, RecordFragment.this.height);
            }

            recordingService.getAddressLiveData().observe(RecordFragment.this, s -> {
                address.setText(s);
                if(s.equals(getString(R.string.disabled_gps))){
                    address.setTextColor(Color.parseColor("#ff0000"));
                }
                else {
                    address.setTextColor(Color.parseColor("#ffffff"));
                }
            });

            isServiceRecording = recordingService.getIsRecordingVideo();
            isServiceRecording.observe(RecordFragment.this,isRecording -> {
                AnimatedVectorDrawable drawable;
                if(isRecording) {
                    drawable = (AnimatedVectorDrawable) getActivity().getDrawable(R.drawable.circle_to_square);
                    chronometer.setBase(recordingService.getStartingTime());
                    chronometer.start();
                    switchCamera.setAlpha(0.38f);
                    muteButton.setAlpha(0.38f);
                    minimizeButton.setAlpha(1.0f);
                    minimizeButton.setEnabled(true);
                }
                else{
                    drawable = (AnimatedVectorDrawable) getActivity().getDrawable(R.drawable.square_to_circle);
                    chronometer.stop();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    switchCamera.setAlpha(1.0f);
                    muteButton.setAlpha(1.0f);
                    minimizeButton.setAlpha(0.38f);
                    minimizeButton.setEnabled(false);
                }
                recordButton.setImageDrawable(drawable);
                switchCamera.setClickable(!isRecording);
                muteButton.setClickable(!isRecording);
                drawable.start();
            });
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            if(getActivity() != null){
                getActivity().unbindService(mConnection);
                navigationController.setServiceUp(false);
                recordingService = null;
            }

        }
    };
    private View decorView;
    private int format;
    private int width;
    private int height;

    public RecordFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment RecordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecordFragment newInstance() {
        RecordFragment fragment = new RecordFragment();

        return fragment;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        if(!this.navigationController.isServiceUp()) {
            intent = new Intent(getActivity(), RecordingService.class);
            getActivity().startService(intent);
            try {
                getActivity().bindService(intent, mConnection, Context.BIND_WAIVE_PRIORITY);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_record, container, false);
        ButterKnife.bind(this, view);

        CameraPreviewContainer.getHolder().addCallback(this);
        recordViewModel = ViewModelProviders.of(this, appViewModelFactory).get(RecordViewModel.class);
        if ( !navigationController.isServiceUp()) {
            doBindService();
        }
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        supportActionBar.setTitle("");

        this.recordViewModel.getDate().observe(this, s -> {
            dateTime.setText(s);
        });
        this.recordViewModel.getVideoDuration().observe(this, duration ->{
            if(duration != null) {
                this.videoMaxDuration.setText(new SimpleDateFormat("mm:ss").format(new Date(duration)));
                chronometer.setOnChronometerTickListener(chronometer -> {
                    Long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
                    if (elapsedMillis > duration) {
                        toggleRecord();
                    }
                });
            }
        });

        recordViewModel.getCurrentCamera().observe(RecordFragment.this, currentCamera -> {
            if( recordingService != null && currentCamera != null ) {
                recordingService.setCurrentCamera();
                recordingService.surfaceChanged(this.format, this.width, this.height);
            }
        });
        this.recordViewModel.getIsSoundEnable().observe(this, (Boolean isSoundEnable) -> {
            if(isSoundEnable != null) {
                if (!isSoundEnable) {
                    this.muteButton.setImageResource(R.drawable.ic_mic_off_black_24dp);
                } else {
                    this.muteButton.setImageResource(R.drawable.ic_mic_black_24dp);
                }
            }
        });

        this.burgerMenu.setOnClickListener(view1 -> {
            ((MainActivity)getActivity()).drawerLayout.openDrawer(Gravity.START);
        });

        this.view.setOnClickListener(view1 -> {
            ((MainActivity)getActivity()).drawerLayout.closeDrawer(Gravity.START);
        });

        ((MainActivity) getActivity()).appBarLayoutVisibility(View.INVISIBLE);
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();

        if (sSurfaceHolder != null) {
            Log.d(TAG, "Sending previous surface");
            //recordingService.surfaceCreated(sSurfaceHolder, false);
            if(recordingService == null){
                doBindService();
            }
            else {
                recordingService.surfaceChanged(this.format, this.width, this.height);
            }
        } else {
            Log.d(TAG, "No previous surface");
        }
        Log.d(TAG, "onResume END");
        hideSystemUI();
    }


    @Override
    public void onPause() {
        Log.d(TAG, "onPause BEGIN");

        showSystemUI();
        super.onPause();

        Log.d(TAG, "onPause END");
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).getDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        ((MainActivity) getActivity()).getDrawerToggle().onConfigurationChanged(newConfig);
    }

    @OnClick(R.id.record)
    public void toggleRecord(){
        if(recordingService == null) return;
        recordingService.toggleRecording();
    }

    @OnClick(R.id.switch_camera)
    public void toggleCamera(){
        if(recordingService == null) return;
        this.recordViewModel.toggleCurrentCamera();
    }

    @OnClick(R.id.mute)
    public void toggleSound(){
        if(recordingService == null) return;
        this.recordingService.toggleSound();
    }

    @OnClick(R.id.minimize_app)
    public void onClickMinimize(){
        Log.d("CAMERA2VIDEOFRAGMENT","onclickminimize");
        navigationController.minimizeApp();
    }



    // This snippet hides the system bars.
    private void hideSystemUI() {

        decorView = getActivity().getWindow().getDecorView();

        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {

        decorView = getActivity().getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        );
    }

    @Override   // SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated holder=" + holder + " (static=" + sSurfaceHolder + ")");
        if (sSurfaceHolder != null) {
            throw new RuntimeException("sSurfaceHolder is already set");
        }

        sSurfaceHolder = holder;
        if(recordingService != null)
            recordingService.surfaceCreated(holder, true);
    }

    @Override   // SurfaceHolder.Callback
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged fmt=" + format + " size=" + width + "x" + height +
                " holder=" + holder);
        this.format = format;
        this.width = width;
        this.height = height;
        if(recordingService != null) {
            recordingService.surfaceChanged(this.format, this.width, this.height);
        }
    }

    @Override   // SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder holder) {
        // In theory we should tell the RenderThread that the surface has been destroyed.
        if(recordingService != null)
            recordingService.surfaceDestroyed();
        Log.d(TAG, "surfaceDestroyed holder=" + holder);
        sSurfaceHolder = null;
    }

}
