package fr.dreamapps.store.kinghul.userInterface.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import javax.inject.Inject;

import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;

/**
 * Created by jdeveaux on 21/09/2017.
 */

public class AutoRemovePreferenceViewModel extends AndroidViewModel {
    public final LiveData<Boolean> preferenceEnabled;
    public final LiveData<Integer> videoQuantity;
    public final LiveData<Boolean> isPremium;
    IPreferencesRepository preferencesRepository;


    @Inject
    public AutoRemovePreferenceViewModel(Application application, IPreferencesRepository preferencesRepository) {
        super(application);
        this.preferencesRepository = preferencesRepository;
        this.preferenceEnabled = this.preferencesRepository.getShouldAutomaticallyRemoveRecording();
        this.isPremium = this.preferencesRepository.isPremium();
        this.videoQuantity = this.preferencesRepository.getVideoQuantityBeforeDeleting();
    }


    public void setPreferenceAutoRemove(boolean b) {
        this.preferencesRepository.setShouldAutomaticallyRemoveRecording(b);
    }

    public void setVideoQuantity(int i){
        this.preferencesRepository.setVideoQuantityBeforeDeleting(i);
    }
}
