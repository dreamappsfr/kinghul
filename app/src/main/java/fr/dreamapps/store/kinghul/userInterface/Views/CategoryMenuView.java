package fr.dreamapps.store.kinghul.userInterface.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.userInterface.Menu.MenuKinghulAdapter;

/**
 * Created by nicolascywier on 28/08/2017.
 */

public class CategoryMenuView extends RelativeLayout {

    @BindView(R.id.category_textview)
    TextView mCategoryTextview;

    public CategoryMenuView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.category_item_menu, this, true);

        ButterKnife.bind(this, itemView);
    }

    public void configure(int position) {
        if (position < MenuKinghulAdapter.SECTION_OTHER_POSITION) {
            mCategoryTextview.setText(R.string.reglages);
            return;
        }
        mCategoryTextview.setText(R.string.autres);
    }
}
