package fr.dreamapps.store.kinghul.dataAccess.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordLocalisationEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.DAO.IDao;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by jdeveaux on 31/07/2017.
 */

@Dao
public abstract class RecordLocalisationDao implements IDao<RecordLocalisationEntity>{

    @Insert(onConflict = REPLACE)
    public abstract long save(RecordLocalisationEntity record);

    @Update
    public abstract void update(RecordLocalisationEntity recordEntity);

    @Delete
    public abstract void delete(RecordLocalisationEntity recordEntity);

    @Query("SELECT * FROM recordLocalisationEntity WHERE id = :id")
    public abstract LiveData<RecordLocalisationEntity> load(int id);

    @Query("SELECT * FROM recordLocalisationEntity")
    public abstract LiveData<List<RecordLocalisationEntity>> load();

    @Query("SELECT * FROM recordLocalisationEntity WHERE record_id = :recordId")
    public abstract LiveData<List<RecordLocalisationEntity>> loadFromRecordId(int recordId);

}
