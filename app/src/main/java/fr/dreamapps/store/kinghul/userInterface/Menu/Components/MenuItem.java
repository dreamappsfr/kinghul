package fr.dreamapps.store.kinghul.userInterface.Menu.Components;

import android.support.annotation.DrawableRes;
import android.view.View;

/**
 * Created by nicolascywier on 27/08/2017.
 */

public class MenuItem {
    private boolean _isPremium;
    private int image;
    private String text;
    private View.OnClickListener callback;

    public MenuItem(@DrawableRes int image, String text, boolean _isPremium, View.OnClickListener callback) {
        this.image = image;
        this._isPremium = _isPremium;
        this.text = text;
        this.callback = callback;
    }

    public int getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    public View.OnClickListener getCallback() {
        return callback;
    }

    public boolean isPremium() {
        return _isPremium;
    }
}
