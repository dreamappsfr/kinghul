package fr.dreamapps.store.kinghul.dataAccess.Entities.Abstract;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by jdeveaux on 31/07/2017.
 */

@Entity
public abstract class AbstractEntity implements Serializable{
    @PrimaryKey(autoGenerate = true)
    private long id;
    public String creationDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        DateFormat df = DateFormat.getDateTimeInstance();
        try {
            return df.parse(creationDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setCreationDate(Date creationDate) {
        DateFormat df = DateFormat.getDateTimeInstance();
        this.creationDate = df.format(creationDate);
    }

}
