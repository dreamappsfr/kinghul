package fr.dreamapps.store.kinghul.userInterface.Fragments;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dependancyInjection.Injectable;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.userInterface.Activities.MainActivity;
import fr.dreamapps.store.kinghul.userInterface.ViewModels.AutoRemovePreferenceViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AutoRemovePreferenceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AutoRemovePreferenceFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory appViewModelFactory;

    @Inject
    IPreferencesRepository preferencesRepository;

    private AutoRemovePreferenceViewModel autoRemovePreferenceViewModel;

    private SwitchCompat enablePreference;

    @BindString(R.string.premium_feature)
    String premiumFeatureText;

    @BindView(R.id.spannable_text)
    TextView spannableText;

    @BindView(R.id.radio_group)
    RadioGroup radioGroup;

    @BindDrawable(R.drawable.ic_star_black_24dp)
    Drawable star;

    @BindColor(R.color.colorPrimary)
    int color;

    public AutoRemovePreferenceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment AutoRemovePreferenceFragment.
     */
    public static AutoRemovePreferenceFragment newInstance() {
        AutoRemovePreferenceFragment fragment = new AutoRemovePreferenceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auto_remove_preference, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity) getActivity()).appBarLayoutVisibility(View.VISIBLE);

        this.setHasOptionsMenu(true);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        supportActionBar.setTitle(R.string.auto_remove);
        supportActionBar.setHomeButtonEnabled(false);
        supportActionBar.setDisplayShowHomeEnabled(false);
        supportActionBar.setDisplayHomeAsUpEnabled(true);

        radioGroup.setOnCheckedChangeListener((radioGroup1, buttonId) -> {
            switch (buttonId){
                case R.id.button_5:
                    autoRemovePreferenceViewModel.setVideoQuantity(5);
                    break;
                case R.id.button_7:
                    autoRemovePreferenceViewModel.setVideoQuantity(7);
                    break;
                case R.id.button_10:
                    autoRemovePreferenceViewModel.setVideoQuantity(10);
                    break;
                default:
                    break;
            }
        });

        star.setBounds(0,0,60,60);
        star.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        ImageSpan imageSpan = new ImageSpan(star, ImageSpan.ALIGN_BOTTOM);

        int start = spannableText.getText().toString().indexOf("#");
        SpannableString spannableString = new SpannableString(spannableText.getText());

        int end = start+1;
        int flag = Spannable.SPAN_EXCLUSIVE_EXCLUSIVE;
        spannableString.setSpan(imageSpan, start, end, flag);

        spannableText.setText(spannableString);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        autoRemovePreferenceViewModel = ViewModelProviders.of(this, appViewModelFactory).get(AutoRemovePreferenceViewModel.class);
        autoRemovePreferenceViewModel.preferenceEnabled.observe(this,(b) -> {
            if(enablePreference != null) {
                enablePreference.setChecked(b);
            }
            if(b){
                Drawable d = ContextCompat.getDrawable(getActivity(),R.color.colorPrimary);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(d);
            }
            else{
                Drawable d = ContextCompat.getDrawable(getActivity(),R.color.disabled_grey);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(d);
            }
        });
        autoRemovePreferenceViewModel.videoQuantity.observe(this,quantity -> {
            switch (quantity){
                case 5:
                    radioGroup.check(R.id.button_5);
                    break;
                case 7:
                    radioGroup.check(R.id.button_7);
                    break;
                case 10:
                    radioGroup.check(R.id.button_10);
                    break;
                default:
                    break;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_preferences, menu);
        RelativeLayout menuLayout = (RelativeLayout) menu.findItem(R.id.myswitch).getActionView();

        enablePreference = menuLayout.findViewById(R.id.switch_preferences);
        enablePreference.setOnTouchListener((view, motionEvent) -> {

            if (!autoRemovePreferenceViewModel.isPremium.getValue() && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                Toast.makeText(getContext(), premiumFeatureText, Toast.LENGTH_SHORT).show();
                return false;
            }
            else if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.performClick();
            }
            return true;
        });

        enablePreference.setChecked(autoRemovePreferenceViewModel.preferenceEnabled.getValue());
        enablePreference.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b != autoRemovePreferenceViewModel.preferenceEnabled.getValue()) {
                autoRemovePreferenceViewModel.setPreferenceAutoRemove(b);
            }
        });
    }

}
