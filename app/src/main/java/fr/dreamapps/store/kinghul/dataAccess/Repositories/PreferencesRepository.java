package fr.dreamapps.store.kinghul.dataAccess.Repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;

import javax.inject.Inject;

import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;

/**
 * Created by jdeveaux on 22/08/2017.
 */

public class PreferencesRepository implements IPreferencesRepository {
    private SharedPreferences sharedPreferences;

    private String SHOULD_RESTART_RECORDING = "SHOULD_RESTART_RECORDING";
    private MutableLiveData<Boolean> _shouldRestartRecording;

    private String IS_PREMIUM = "IS_PREMIUM";
    private MutableLiveData<Boolean> _isPremium;

    private String AUTO_RECORD = "AUTO_RECORD";
    private MutableLiveData<Boolean> _autoRecord;

    private String SHOULD_AUTOMATICALLY_REMOVE_RECORDING = "SHOULD_AUTOMATICALLY_REMOVE_RECORDING";
    private MutableLiveData<Boolean> _shouldAutomaticallyRemoveRecording;

    private String VIDEO_QUANTITY_BEFORE_DELETING = "VIDEO_QUANTITY_BEFORE_DELETING";
    private MutableLiveData<Integer> _videoQuantityBeforeDeleting;

    private String VIDEO_DURATION_KEY = "VIDEO_DURATION_KEY";
    private MutableLiveData<Long> _videoDuration;

    private String ENABLE_SOUND = "ENABLE_SOUND";
    private MutableLiveData<Boolean> _enableSound;


    private String PREFERED_CAMERA = "PREFERED_CAMERA";
    private MutableLiveData<Integer> _preferedCameraIndex;

    @Inject
    public PreferencesRepository(SharedPreferences sharedPreferences) {
        this._videoDuration = new MutableLiveData<>();
        this._shouldRestartRecording = new MutableLiveData<>();
        this._autoRecord = new MutableLiveData<>();
        this._isPremium = new MutableLiveData<>();
        this._shouldAutomaticallyRemoveRecording = new MutableLiveData<>();
        this._enableSound = new MutableLiveData<>();
        this._videoQuantityBeforeDeleting = new MutableLiveData<>();
        this._preferedCameraIndex = new MutableLiveData<>();
        this.sharedPreferences = sharedPreferences;
        this._isPremium.setValue(false);
        this._videoDuration.setValue(this.sharedPreferences.getLong(VIDEO_DURATION_KEY, 180000)); // Set to 3min by default (180s)
        this._shouldRestartRecording.setValue(this.sharedPreferences.getBoolean(SHOULD_RESTART_RECORDING, true));
        this._shouldAutomaticallyRemoveRecording.setValue(this.sharedPreferences.getBoolean(SHOULD_AUTOMATICALLY_REMOVE_RECORDING, false));
        this._autoRecord.setValue(this.sharedPreferences.getBoolean(AUTO_RECORD, false));
        this._enableSound.setValue(this.sharedPreferences.getBoolean(ENABLE_SOUND, true));
        this._videoQuantityBeforeDeleting.setValue(this.sharedPreferences.getInt(VIDEO_QUANTITY_BEFORE_DELETING,5));
        this._preferedCameraIndex.setValue(this.sharedPreferences.getInt(PREFERED_CAMERA,0));
    }

    @Override
    public LiveData<Integer> getVideoQuantityBeforeDeleting() {
        return _videoQuantityBeforeDeleting;
    }

    @Override
    public LiveData<Integer> getPreferedCamera() {
        return _preferedCameraIndex;
    }

    @Override
    public void setPreferedCamera(int value) {
        this.sharedPreferences.edit().putInt(PREFERED_CAMERA, value).apply();
        this._preferedCameraIndex.setValue(value);
    }

    @Override
    public LiveData<Long> getVideoDuration(){
        return _videoDuration;
    }

    @Override
    public void setVideoDuration(long value){
        this.sharedPreferences.edit().putLong(VIDEO_DURATION_KEY, value).apply();
        this._videoDuration.setValue(value);
    }

    @Override
    public LiveData<Boolean> getShouldRestartRecording() {
        return _shouldRestartRecording;
    }

    @Override
    public void setShouldRestartRecording(boolean value) {
        this.sharedPreferences.edit().putBoolean(SHOULD_RESTART_RECORDING, value).apply();
        this._shouldRestartRecording.setValue(value);
    }

    @Override
    public LiveData<Boolean> getShouldAutomaticallyRemoveRecording() {
        return _shouldAutomaticallyRemoveRecording;
    }

    @Override
    public void setShouldAutomaticallyRemoveRecording(boolean value) {
        this.sharedPreferences.edit().putBoolean(SHOULD_AUTOMATICALLY_REMOVE_RECORDING, value).apply();
        this._shouldAutomaticallyRemoveRecording.setValue(value);
    }

    @Override
    public LiveData<Boolean> getEnableSound() {
        return _enableSound;
    }

    @Override
    public void setEnableSound(Boolean value) {
        this.sharedPreferences.edit().putBoolean(ENABLE_SOUND, value).apply();
        this._enableSound.setValue(value);
    }

    @Override
    public LiveData<Boolean> getAutoRecord() {
        return _autoRecord;
    }

    @Override
    public void setAutoRecord(boolean value) {
        this.sharedPreferences.edit().putBoolean(AUTO_RECORD, value).apply();
        this._autoRecord.setValue(value);
    }

    @Override
    public void setVideoQuantityBeforeDeleting(int quantity) {
        this.sharedPreferences.edit().putInt(VIDEO_QUANTITY_BEFORE_DELETING, quantity).apply();
        this._videoQuantityBeforeDeleting.setValue(quantity);
    }

    @Override
    public void setPremiumEnabled(boolean b) {
        this.sharedPreferences.edit().putBoolean(IS_PREMIUM, b).apply();
        _isPremium.setValue(b);
    }

    @Override
    public LiveData<Boolean> isPremium() {
        return _isPremium;
    }
}
