package fr.dreamapps.store.kinghul.userInterface.Menu.Components;

import android.view.View;

/**
 * Created by jdeveaux on 28/08/2017.
 */

public class ButtonMenuItem extends MenuItem {
    public ButtonMenuItem(int image, String text, boolean isPremium, View.OnClickListener callback) {
        super(image, text, isPremium, callback);
    }
}
