package fr.dreamapps.store.kinghul.userInterface.Menu.Components;

import android.arch.lifecycle.LiveData;
import android.view.View;
import android.widget.CompoundButton;

/**
 * Created by jdeveaux on 28/08/2017.
 */

public class SwitchMenuItem extends MenuItem {
    private LiveData<Boolean> checked;
    private View.OnTouchListener checkedChangeListener;
    private View.OnClickListener clickListener;

    public SwitchMenuItem(int image, String text, LiveData<Boolean> switchValue, boolean isPremium, View.OnTouchListener checkedChangeListener, View.OnClickListener clickListener) {
        super(image, text,isPremium, null);
        this.checked = switchValue;
        this.checkedChangeListener = checkedChangeListener;
        this.clickListener = clickListener;
    }

    public LiveData<Boolean> isChecked() {
        return checked;
    }

    public View.OnTouchListener getCheckedChangeListener() {
        return checkedChangeListener;
    }

    public View.OnClickListener getClickListener(){
        return clickListener;
    }
}
