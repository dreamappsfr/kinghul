package fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories;

import android.arch.lifecycle.LiveData;

/**
 * Created by jdeveaux on 22/08/2017.
 */

public interface IPreferencesRepository {
    LiveData<Integer> getVideoQuantityBeforeDeleting();

    LiveData<Integer> getPreferedCamera();

    void setPreferedCamera(int value);

    LiveData<Long> getVideoDuration();

    void setVideoDuration(long value);

    LiveData<Boolean> getShouldRestartRecording();

    void setShouldRestartRecording(boolean value);

    LiveData<Boolean> getShouldAutomaticallyRemoveRecording();

    void setShouldAutomaticallyRemoveRecording(boolean value);

    LiveData<Boolean> getEnableSound();

    void setEnableSound(Boolean value);

    LiveData<Boolean> getAutoRecord();

    void setAutoRecord(boolean value);

    void setVideoQuantityBeforeDeleting(int quantity);

    void setPremiumEnabled(boolean b);

    LiveData<Boolean> isPremium();
}
