package fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.Abstract;

import android.arch.lifecycle.LiveData;

import java.util.List;

/**
 * Created by jdeveaux on 31/07/2017.
 */

public interface IAbstractRepository<T> {
    LiveData<T> get(int id);
    LiveData<List<T>> get();
}
