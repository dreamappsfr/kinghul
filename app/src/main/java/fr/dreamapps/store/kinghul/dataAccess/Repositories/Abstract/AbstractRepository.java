package fr.dreamapps.store.kinghul.dataAccess.Repositories.Abstract;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Singleton;

import fr.dreamapps.store.kinghul.dataAccess.Entities.Abstract.AbstractEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.DAO.IDao;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.Abstract.IAbstractRepository;

/**
 * Created by jdeveaux on 31/07/2017.
 */

@Singleton
public abstract class AbstractRepository<T extends AbstractEntity,DAOT extends IDao<T>> implements IAbstractRepository<T>{
    protected DAOT dao;

    public LiveData<T> get(int id) {
        // return a LiveData directly from the database.
        return dao.load(id);
    }

    public LiveData<List<T>> get() {
        // return a LiveData directly from the database.
        return dao.load();
    }


}
