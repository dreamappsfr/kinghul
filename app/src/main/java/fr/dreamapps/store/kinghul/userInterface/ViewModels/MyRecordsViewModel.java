package fr.dreamapps.store.kinghul.userInterface.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import fr.dreamapps.store.kinghul.R;
import fr.dreamapps.store.kinghul.dataAccess.Entities.RecordEntity;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;

/**
 * Created by jdeveaux on 21/09/2017.
 */

public class MyRecordsViewModel extends AndroidViewModel {
    IPreferencesRepository preferencesRepository;
    IRecordRepository recordRepository;

    public LiveData<List<RecordEntity>> getRecords() {
        return records;
    }

    LiveData<List<RecordEntity>> records;
    String videoSaved;
    String videoNotSaved;

    @Inject
    public MyRecordsViewModel(Application application, IPreferencesRepository preferencesRepository, IRecordRepository recordRepository) {
        super(application);
        this.preferencesRepository = preferencesRepository;
        this.recordRepository = recordRepository;
        records = recordRepository.get();
        videoSaved = application.getResources().getString(R.string.video_starred);
        videoNotSaved = application.getResources().getString(R.string.video_not_starred);
    }

    public void setSaveFor(RecordEntity recordEntity, boolean b) {
        if(b){
            Toast.makeText(getApplication(), videoSaved , Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplication(), videoNotSaved , Toast.LENGTH_LONG).show();
        }
        recordEntity.setSaved(b);
        recordRepository.update(recordEntity);
    }

    public void updatePathFor(RecordEntity recordEntity, String oldPath) {
        recordRepository.update(recordEntity);
        File from = new File(oldPath);
        File to = new File(recordEntity.getPath());
        if (from.exists()){
            from.renameTo(to);
        }
    }

    public void deleteRecord(RecordEntity recordEntity){
        recordRepository.delete(recordEntity);
    }
}
