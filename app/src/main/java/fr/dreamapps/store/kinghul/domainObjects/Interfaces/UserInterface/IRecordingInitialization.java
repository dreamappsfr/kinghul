package fr.dreamapps.store.kinghul.domainObjects.Interfaces.UserInterface;

/**
 * Created by jdeveaux on 16/09/2017.
 */

public interface IRecordingInitialization {
    void onComplete();
}
