/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.dreamapps.store.kinghul.dependancyInjection;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.dreamapps.store.kinghul.dataAccess.DAO.RecordDao;
import fr.dreamapps.store.kinghul.dataAccess.DAO.RecordLocalisationDao;
import fr.dreamapps.store.kinghul.dataAccess.PersistDB;
import fr.dreamapps.store.kinghul.dataAccess.Repositories.PreferencesRepository;
import fr.dreamapps.store.kinghul.dataAccess.Repositories.RecordRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IPreferencesRepository;
import fr.dreamapps.store.kinghul.domainObjects.Interfaces.Repositories.IRecordRepository;

@Module(includes = ViewModelModule.class)
class AppModule {

    private String SHARED_PREFERENCES_FILENAME = "fr.dreamapps.store.kinghul.sharedprefs";

    @Singleton @Provides
    PersistDB provideDb(Application app) {
        return Room.databaseBuilder(app, PersistDB.class,"kinghul.db").build();
    }

    @Singleton @Provides
    RecordDao provideRecordDao(PersistDB db) {
        return db.recordDao();
    }


    @Singleton @Provides
    RecordLocalisationDao provideRecordLocalisationDao(PersistDB db) {
        return db.recordLocalisationDao();
    }

    @Singleton @Provides
    IRecordRepository provideRecordRepository(RecordDao recordDao,RecordLocalisationDao recordLocalisationDao, IPreferencesRepository preferencesRepository, Application application) {
        return new RecordRepository(recordDao, recordLocalisationDao, preferencesRepository, application);
    }

    @Singleton @Provides
    IPreferencesRepository providePreferencesRepository(SharedPreferences sharedPreferences) {
        return new PreferencesRepository(sharedPreferences);
    }

    @Singleton @Provides
    SharedPreferences provideSharedPreferences(Application app){
        return app.getSharedPreferences(SHARED_PREFERENCES_FILENAME, Context.MODE_PRIVATE);
    }

}
